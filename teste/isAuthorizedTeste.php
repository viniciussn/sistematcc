<?php


use PHPUnit\Framework\TestCase;
include("Teste.php");

class isAuthorizedTeste extends TestCase{
	
	public function testIsAuthorized(){
		$projetoController = new ProjetoController();

		$this->assertEquals(true, $projetoController->isAuthorized('index',3,null,1));
		$this->assertEquals(false, $projetoController->isAuthorized('add',3,null,1));
		$this->assertEquals(false, $projetoController->isAuthorized('delete',3,1,1));
		$this->assertEquals(true, $projetoController->isAuthorized('find',3,1,1));
		$this->assertEquals(false, $projetoController->isAuthorized('update',3,1,1));
		$this->assertEquals(true, $projetoController->isAuthorized('index',2,null,1));
		$this->assertEquals(true, $projetoController->isAuthorized('add',2,null,1));
		$this->assertEquals(true, $projetoController->isAuthorized('delete',2,1,1));
		$this->assertEquals(true, $projetoController->isAuthorized('find',2,1,1));
		$this->assertEquals(true, $projetoController->isAuthorized('update',2,1,1));
		$this->assertEquals(false, $projetoController->isAuthorized('delete',2,2,1));
		$this->assertEquals(false, $projetoController->isAuthorized('find',2,2,1));
		$this->assertEquals(false, $projetoController->isAuthorized('update',2,2,1));
		$this->assertEquals(true, $projetoController->isAuthorized('index',1,null,1));
		$this->assertEquals(true, $projetoController->isAuthorized('add',1,null,1));
		$this->assertEquals(true, $projetoController->isAuthorized('delete()',1,1,1));
		$this->assertEquals(true, $projetoController->isAuthorized('find',1,1,1));
		$this->assertEquals(true, $projetoController->isAuthorized('update',1,1,1));
	}
}
?>