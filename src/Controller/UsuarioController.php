<?php

namespace App\Controller;
use Cake\Auth\DefaultPasswordHasher;
class UsuarioController extends AppController {
	public function isAuthorized(){
		return true;
	}
	public function index() {
		$this->viewBuilder()->setLayout('basic_layout');
	}
	public function login() {
		$this->viewBuilder()->setLayout('basic_layout');
		if ($this->request->is('post')) {
			/*usando o método identify sempre retorna false*/
			//$usuario = $this->Auth->identify();
			$usuario = $this->Usuario->find('all')->where(['login'=>$this->request->getData()['login']])->first();
			if ($usuario) {
				if ((new DefaultPasswordHasher)->check($this->request->getData()['password'], $usuario->password)) {
					$this->Auth->setUser($usuario);
					$this->request->session()->write('user.tipo', $usuario->tipo);
					if($usuario->tipo==3){
						$this->loadModel('Aluno');
						$aluno = $this->Aluno->find('all')->where(['usuario_idUsuario'=>$usuario->idUsuario])->first();						
						$this->request->session()->write('class.id', $aluno->idAluno);
					}else{
						$this->loadModel('Professor');
						$professor = $this->Professor->find('all')->where(['usuario_idUsuario'=>$usuario->idUsuario])->first();						
						$this->request->session()->write('class.id', $professor->idProfessor);
					}
					return $this->redirect($this->Auth->redirectUrl());
				}
			}
			echo "Login ou senha incorretos!";

		}
	}

	public function logout(){
		return $this->redirect($this->Auth->logout());
	}
}
?>
