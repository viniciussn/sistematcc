<?php

namespace App\Controller;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
class EncontroController extends AppController {
	
	public function isAuthorized(){		
		$session = $this->request->session();
		$user_tipo = $session->read('user.tipo');
		$this->loadModel('Projeto');
		if ($this->request->getParam('action') === 'proximos'){
			return true;
		}
		if($session->read('user.tipo')==3){
			if ($this->request->getParam('action') === 'index') {
				if ($this->Projeto->exists(['idProjeto'=>$this->request->getParam('pass')[0],'aluno_idaluno'=> $session->read('class.id')])) {
					return true;
				}
			}else 	if ($this->request->getParam('action') === 'find') {
				if ($this->Projeto->exists(['idProjeto'=>$this->request->data['projeto_idProjeto'],'aluno_idaluno'=> $session->read('class.id')])) {
					return true;
				}
			}
		}else  if($session->read('user.tipo')==2){
			if ($this->request->getParam('action') === 'index') {
				if ($this->Projeto->exists(['idProjeto'=> $this->request->getParam('pass')[0],'professor_orientador1'=> $session->read('class.id')]) or $this->Projeto->exists(['idProjeto'=> $this->request->getParam('pass')[0],'professor_orientador2'=> $session->read('class.id')])) {
					return true;
				}
			}else{
				if ($this->Projeto->exists(['idProjeto'=> $this->request->data['projeto_idProjeto'],'professor_orientador1'=> $session->read('class.id')]) or $this->Projeto->exists(['idProjeto'=> $this->request->data['projeto_idProjeto'],'professor_orientador2'=> $session->read('class.id')])) {
					return true;
				}
			}
		}
		return parent::isAuthorized();
	}

	public function index($projeto_idProjeto) {
		$this->loadModel('Projeto');
		$projeto = $this->Projeto->get($projeto_idProjeto);			
		$this->viewBuilder()->setLayout('basic_layout');
		$encontro = $this->Encontro->find('all', array('order' => "data", 'contain'=>array('Projeto')))->where(['projeto_idProjeto' => $projeto_idProjeto]);
		$this->set(compact('encontro','projeto'));	
	}

	public function proximos(){
		$this->viewBuilder()->setLayout('basic_layout');
		$this->loadModel('Projeto');
		$this->loadModel('Professor');
		$this->loadModel('Aluno');
		$session = $this->request->session();
		$user_tipo = $session->read('user.tipo');
		$id_user = $session->read('class.id');

		if($user_tipo==3){
			$conn = ConnectionManager::get('default');
			$stmt = $conn->execute("SELECT * from encontro NATURAL JOIN projeto  WHERE projeto.aluno_idaluno ==".$id_user);
			$encontros = $stmt ->fetchAll('assoc');		
			$projetos= $this->Projeto->find('all', array('order' => "tituloProjeto", 'contain'=>array('Aluno','Professor1','Professor2','Status')))->where(
				['aluno_idaluno'=>$session->read('class.id')])->toArray();
		}else{
			$conn = ConnectionManager::get('default');
			$stmt = $conn->execute("SELECT * from encontro NATURAL JOIN projeto WHERE  professor_orientador1 =".$id_user." OR professor_orientador2 = ".$id_user);
			$encontros = $stmt ->fetchAll('assoc');		
			$projetos= $this->Projeto->find('all', array('order' => "tituloProjeto", 'contain'=>array('Aluno','Professor1','Professor2','Status')))->where([
				'OR'=>['professor_orientador1'=>$session->read('class.id'),'professor_orientador2'=>$session->read('class.id')]])->toArray();	
		}

		
		$this->set(compact('encontros','projetos'));	
	}

	public function add(){
		$this->autoRender = false;
		$this->response->type('json');
		$encontro = $this->Encontro->newEntity();
		if ($this->request->is('post')) {
			$encontro = $this->Encontro->patchEntity($encontro, $this->request->getData());	
			if ($this->Encontro->save($encontro)) {
				$this->response->statusCode(200);
				$result = $this->Encontro->find('all')->where(['idEncontro'=>$encontro->idEncontro])->contain(['Projeto'])->first();
				$this->response->body(json_encode(array('result' => 'success','idEncontro'=>$result->idEncontro, 'temaEncontro'=>$result->temaEncontro,'data'=>$result->data->format('m/d/Y'),'hora'=>$result->hora->format('h:m'),'local'=>$result->local)));
			}else{
				$this->response->statusCode(200);
				$this->response->body(json_encode(array('result' => 'error')));
			}
		}
		return $this->response;
	}
	public function delete()    {
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idEncontro = $this->request->data['idEncontro'];
		try {
			$encontro = $this->Encontro->get($idEncontro);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		
		if($this->Encontro->delete($encontro)){			
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'success')));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}

	public function update(){
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idEncontro = $this->request->data['idEncontro'];
		try {
			$encontro = $this->Encontro->get($idEncontro);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		
		$encontro->temaEncontro = $this->request->data['temaEncontro'];
		$encontro->descEncontro = $this->request->data['descEncontro'];
		$encontro->atividades = $this->request->data['atividades'];
		$encontro->problemas = $this->request->data['problemas'];
		$encontro->atividadesFuturas = $this->request->data['atividadesFuturas'];
		$encontro->hora = $this->request->data['hora'];
		$encontro->data = $this->request->data['data'];
		$encontro->local = $this->request->data['local'];		
		if($this->Encontro->save($encontro)){							
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'success','idEncontro'=>$encontro->idEncontro, 'temaEncontro'=>$encontro->temaEncontro,
				'data'=>date('m/d/Y',strtotime($encontro->datga)),'hora'=>date('h:m',strtotime($encontro->hora)),'local'=>$encontro->local)));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}
	public function find(){
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idEncontro = $this->request->data['idEncontro'];
		try {
			$encontro = $this->Encontro->get($idEncontro);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		$this->response->statusCode(200);
		$result = $this->Encontro->find('all')->where(['idEncontro'=>$encontro->idEncontro])->contain(['Projeto'])->first();
		$this->response->body(json_encode(array('result' => 'success','idEncontro'=>$result->idEncontro, 'temaEncontro'=>$result->temaEncontro,'descEncontro'=>$result->descEncontro,'data'=>$result->data->format('Y-m-d'), 'atividades'=>$result->atividades,'atividadesFuturas'=>$result->atividadesFuturas,'problemas'=>$result->problemas,'hora'=>$result->hora->format('h:m'),'local'=>$result->local,'projeto_idProjeto'=>$result->projeto_idProjeto)));
		return $this->response;
	}

}

?>