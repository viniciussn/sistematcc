<?php

namespace App\Controller;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;

class AlunoController extends AppController {

	public function isAuthorized(){		
		return parent::isAuthorized();
	}


	public function index() {
		$this->viewBuilder()->setLayout('basic_layout');	
		$aluno= $this->Aluno->find('all', array('order' => "nomeAluno"));
		$this->set(compact('aluno'));

	}
	public function add()    {
		
		$this->autoRender = false;
		$this->response->type('json');
		if ($this->request->is('post')) {

			$this->loadModel('Usuario');				
			$conn = ConnectionManager::get('default');
			$conn->begin();

			$usuario = $this->Usuario->newEntity([
				'login' => $this->request->getData()['emailAluno'],
				'password' => (new DefaultPasswordHasher())->hash($this->request->getData()['senhaAluno']),
				'tipo' => 3,
			]);

			if($this->Usuario->save($usuario)){
				$aluno = $this->Aluno->newEntity([
					'nomeAluno' => $this->request->getData()['nomeAluno'],
					'telefoneAluno' => $this->request->getData()['telefoneAluno'],
					'emailAluno' => $this->request->getData()['emailAluno'],
				]);		
				$aluno ->usuario_idUsuario = $usuario->idUsuario;
				if ($this->Aluno->save($aluno)) {
					$this->response->statusCode(200);
					$this->response->body(json_encode(array('result' => 'success','id'=>$aluno->idAluno, 'nome'=>$aluno->nomeAluno,
						'email'=>$aluno->emailAluno,'telefone'=>$aluno->telefoneAluno)));
					$conn->commit();
				}else{					
					$conn->rollback();	
					$this->response->statusCode(200);
					$this->response->body(json_encode(array('result' => 'error')));				
				}
			}else{
				$this->response->statusCode(200);
				$this->response->body(json_encode(array('result' => 'error')));
			}
		}
		return $this->response;
	}

	public function delete()    {
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idAluno = $this->request->data['idAluno'];
		try {
			$aluno = $this->Aluno->get($idAluno);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}

		if($this->Aluno->delete($aluno)){			
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'success')));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}

	public function update(){
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idAluno = $this->request->data['idAluno'];
		try {
			$aluno = $this->Aluno->get($idAluno);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		$aluno->nomeAluno = $this->request->data['nomeAluno'];
		$aluno->emailAluno = $this->request->data['emailAluno'];
		$aluno->telefoneAluno = $this->request->data['telefoneAluno'];
		if($this->Aluno->save($aluno)){			
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'success','id'=>$aluno->idAluno, 'nome'=>$aluno->nomeAluno,
				'email'=>$aluno->emailAluno,'telefone'=>$aluno->telefoneAluno)));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}

}

?>