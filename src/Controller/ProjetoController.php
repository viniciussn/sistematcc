<?php 
namespace App\Controller;

class ProjetoController extends AppController {

	public function isAuthorized(){

		$session = $this->request->session();
		$user_tipo = $session->read('user.tipo');

		if ($this->request->getParam('action') === 'index') {
			return true;			
		}
		
		if ($this->request->getParam('action') === 'add') {
			if($user_tipo==3 ){
				return false;	
			}
			return true;					
		}

		if (in_array($this->request->getParam('action'), ['update', 'delete'])) {
			$idProjeto = $this->request->data['idProjeto'];
			if ($this->Projeto->exists(['idProjeto'=>$idProjeto,'professor_orientador1'=> $session->read('class.id')]) or $this->Projeto->exists(['idProjeto'=>$idProjeto,'professor_orientador2'=> $session->read('class.id')])) {
				return true;
			}			
		}

		if ($this->request->getParam('action') === 'find') {
			$idProjeto = $this->request->data['idProjeto'];
			if($session->read('user.tipo')==2){
				if ($this->Projeto->exists(['idProjeto'=>$idProjeto,'professor_orientador1'=> $session->read('class.id')]) or $this->Projeto->exists(['idProjeto'=>$idProjeto,'professor_orientador2'=> $session->read('class.id')])) {
					return true;
				}	
			}else if($session->read('user.tipo')==3){
				if ($this->Projeto->exists(['idProjeto'=>$idProjeto,'aluno_idaluno'=> $session->read('class.id')])) {
					return true;
				}	
			}				
		}

		return parent::isAuthorized();
	}
	public function index() {
		$session = $this->request->session();
		$this->loadModel('Aluno');
		$this->loadModel('Professor');
		$this->loadModel('Status');
		$status = $this->Status->find('all', array('order' => "nomeStatus"));
		$alunos = $this->Aluno->find('all', array('order' => "nomeAluno"));
		$professores = $this->Professor->find('all', array('order' => "nomeProfessor"));
		$this->viewBuilder()->setLayout('basic_layout');
		if($session->read('user.tipo')==3){
			$projetos= $this->Projeto->find('all', array('order' => "tituloProjeto", 'contain'=>array('Aluno','Professor1','Professor2','Status')))->where(
				['aluno_idaluno'=>$session->read('class.id')])->toArray();
		}
		if($session->read('user.tipo')==2){
			$projetos= $this->Projeto->find('all', array('order' => "tituloProjeto", 'contain'=>array('Aluno','Professor1','Professor2','Status')))->where([
				'OR'=>['professor_orientador1'=>$session->read('class.id'),'professor_orientador2'=>$session->read('class.id')]])->toArray();
		}else if($session->read('user.tipo')==1){
			$projetos= $this->Projeto->find('all', array('order' => "tituloProjeto", 'contain'=>array('Aluno','Professor1','Professor2','Status')))->toArray();
		}
		
		
		$this->set(compact('projetos','alunos','professores','status'));
	}

	public function add()    {
		$this->autoRender = false;
		$this->response->type('json');
		$projeto = $this->Projeto->newEntity();

		if ($this->request->is('post')) {
			$projeto = $this->Projeto->patchEntity($projeto, $this->request->getData());
			if ($this->Projeto->save($projeto)) {
				$this->response->statusCode(200);
				$result = $this->Projeto->find('all')->where(['idProjeto'=>$projeto->idProjeto])->contain(['Aluno','Professor1','Professor2','Status'])->first();
				$this->response->body(json_encode(array('result' => 'success','idProjeto'=>$result->idProjeto, 'tituloProjeto'=>$result->tituloProjeto,'professor'=>$result->professor1->nomeProfessor,'aluno'=>$result->aluno->nomeAluno,'status'=>$result->status->nomeStatus)));
			}else{
				$this->response->statusCode(200);
				$this->response->body(json_encode(array('result' => 'error')));
			}
		}
		return $this->response;
	}
	public function delete()    {
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idProjeto = $this->request->data['idProjeto'];
		try {
			$projeto = $this->Projeto->get($idProjeto);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		
		if($this->Projeto->delete($projeto)){			
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'success')));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}

	public function update(){
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idProjeto = $this->request->data['idProjeto'];
		
		try {
			$projeto = $this->Projeto->get($idProjeto);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}

		$projeto->tituloProjeto = $this->request->data['tituloProjeto'];
		$projeto->descProjeto = $this->request->data['descProjeto'];
		$projeto->dtInicio = $this->request->data['dtInicio'];
		$projeto->dtFim = $this->request->data['dtFim'];
		$projeto->status_idstatus = $this->request->data['status_idstatus'];
		$projeto->professor_orientador1 = $this->request->data['professor_orientador1'];
		$projeto->professor_orientador2 = $this->request->data['professor_orientador2'];

		if($this->Projeto->save($projeto)){			
			$this->response->statusCode(200);
			$result = $this->Projeto->find('all')->where(['idProjeto'=>$projeto->idProjeto])->contain(['Aluno','Professor1','Professor2','Status'])->first();
			$this->response->body(json_encode(array('result' => 'success','idProjeto'=>$result->idProjeto, 'tituloProjeto'=>$result->tituloProjeto,'professor'=>$result->professor1->nomeProfessor,'aluno'=>$result->aluno->nomeAluno,'status'=>$result->status->nomeStatus)));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}

	public function find(){
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idProjeto = $this->request->data['idProjeto'];
		try {
			$projeto = $this->Projeto->get($idProjeto);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		$this->response->statusCode(200);
		$result = $this->Projeto->find('all')->where(['idProjeto'=>$projeto->idProjeto])->contain(['Aluno','Professor1','Professor2','Status'])->first();
		$this->response->body(json_encode(array('result' => 'success','idProjeto'=>$result->idProjeto, 'tituloProjeto'=>$result->tituloProjeto,'descProjeto'=>$result->descProjeto,'dtInicio'=>$result->dtInicio->format('Y-m-d'), 'dtFim'=>$result->dtFim->format('Y-m-d'),'professor_orientador1'=>$result->professor_orientador1,'professor_orientador2'=>$result->professor_orientador2,'aluno_idaluno'=>$result->aluno_idaluno,'status_idstatus'=>$result->status_idstatus,'nomeStatus'=>$result->status->nomeStatus,'nomeProfessor2'=>$result->professor2->nomeProfessor,'nomeAluno'=>$result->aluno->nomeAluno,'nomeProfessor1'=>$result->professor1->nomeProfessor)));
		return $this->response;
	}
}
?>