<?php

namespace App\Controller;
use Cake\Auth\DefaultPasswordHasher;

class EtapaController extends AppController {

	public function isAuthorized(){		
		$session = $this->request->session();
		$user_tipo = $session->read('user.tipo');
		$this->loadModel('Projeto');
		if($session->read('user.tipo')==3){
			if ($this->request->getParam('action') === 'index') {
				if ($this->Projeto->exists(['idProjeto'=>$this->request->getParam('pass')[0],'aluno_idaluno'=> $session->read('class.id')])) {
					return true;
				}
			}else 	if ($this->request->getParam('action') === 'find') {
				if ($this->Projeto->exists(['idProjeto'=>$this->request->data['projeto_idProjeto'],'aluno_idaluno'=> $session->read('class.id')])) {
					return true;
				}
			}
		}else  if($session->read('user.tipo')==2){
			if ($this->request->getParam('action') === 'index') {
				if ($this->Projeto->exists(['idProjeto'=> $this->request->getParam('pass')[0],'professor_orientador1'=> $session->read('class.id')]) or $this->Projeto->exists(['idProjeto'=> $this->request->getParam('pass')[0],'professor_orientador2'=> $session->read('class.id')])) {
					return true;
				}
			}else{
				if ($this->Projeto->exists(['idProjeto'=> $this->request->data['projeto_idProjeto'],'professor_orientador1'=> $session->read('class.id')]) or $this->Projeto->exists(['idProjeto'=> $this->request->data['projeto_idProjeto'],'professor_orientador2'=> $session->read('class.id')])) {
					return true;
				}
			}
		}
		return parent::isAuthorized();
	}

	public function index($projeto_idProjeto) {
		$this->loadModel('StatusEtapa');
		$this->loadModel('Projeto');
		$projeto = $this->Projeto->get($projeto_idProjeto);
		$status = $this->StatusEtapa->find('all', array('order' => "nomeStatus"));
		$this->viewBuilder()->setLayout('basic_layout');
		$etapa = $this->Etapa->find('all', array('order' => "dtInicio", 'contain'=>array('StatusEtapa')))->where(['projeto_idProjeto' => $projeto_idProjeto]);
		$this->set(compact('etapa','status','projeto'));	
	}
	public function add()    {

		$this->autoRender = false;
		$this->response->type('json');
		$etapa = $this->Etapa->newEntity();
		if ($this->request->is('post')) {
			$etapa = $this->Etapa->patchEntity($etapa, $this->request->getData());			
			$this->log($this->request->getData(), 'debug');
			if ($this->Etapa->save($etapa)) {
				$this->response->statusCode(200);
				$result = $this->Etapa->find('all')->where(['idEtapa'=>$etapa->idEtapa])->contain(['Projeto','StatusEtapa'])->first();
				$this->response->body(json_encode(array('result' => 'success','idEtapa'=>$result->idEtapa, 'nomeEtapa'=>$result->nomeEtapa,'dtInicio'=>$result->dtInicio->format('d-m-Y'),'status'=>$result->status_etapa->nomeStatus)));
			}else{
				$this->response->statusCode(200);
				$this->response->body(json_encode(array('result' => 'error')));
			}
		}
		return $this->response;
	}
	public function delete()    {
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idEtapa = $this->request->data['idEtapa'];
		try {
			$etapa = $this->Etapa->get($idEtapa);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		
		if($this->Etapa->delete($etapa)){			
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'success')));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}

	public function update(){
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idEtapa = $this->request->data['idEtapa'];
		try {
			$etapa = $this->Etapa->get($idEtapa);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		
		$etapa->nomeEtapa = $this->request->data['nomeEtapa'];
		$etapa->descEtapa = $this->request->data['descEtapa'];
		$etapa->dtInicio = $this->request->data['dtInicio'];
		$etapa->dtFim = $this->request->data['dtFim'];
		$etapa->comentarioEtapa = $this->request->data['comentarioEtapa'];
		$etapa->statusEtapa_idStatus = $this->request->data['statusEtapa_idStatus'];
		$this->loadModel('StatusEtapa');
		$status = $this->StatusEtapa->get($etapa->statusEtapa_idStatus);
		if($this->Etapa->save($etapa)){							
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'success','idEtapa'=>$etapa->idEtapa, 'nomeEtapa'=>$etapa->nomeEtapa,
				'dtInicio'=>date('m/d/Y',strtotime($etapa->dtInicio)),'status'=>$status->nomeStatus)));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}
	public function find(){
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idEtapa = $this->request->data['idEtapa'];
		try {
			$etapa = $this->Etapa->get($idEtapa);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		$this->response->statusCode(200);
		$result = $this->Etapa->find('all')->where(['idEtapa'=>$etapa->idEtapa])->contain(['StatusEtapa','Projeto'])->first();

		$dtInicio = $result->dtInicio==null ? null :$result->dtInicio->format('Y-m-d');
		$dtFim =  $result->dtFim==null ? null :$result->dtFim->format('Y-m-d');
		$this->response->body(json_encode(array('result' => 'success','idEtapa'=>$result->idEtapa, 'nomeEtapa'=>$result->nomeEtapa,'descEtapa'=>$result->descEtapa,'dtInicio'=>$dtInicio, 'dtFim'=>$dtFim,'comentarioEtapa'=>$result->comentarioEtapa,'nomeStatus'=>$result->status_etapa->nomeStatus,'statusEtapa_idStatus'=>$result->status_etapa->idStatus,'nomeProjeto'=>$result->projeto->tituloProjeto)));
		return $this->response;
	}

}

?>