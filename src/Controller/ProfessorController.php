<?php 
namespace App\Controller;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;

class ProfessorController extends AppController {
							
	public function isAuthorized(){		
		return parent::isAuthorized();
	}
	
	public function index() {
		$this->viewBuilder()->setLayout('basic_layout');
		$professor = $this->Professor->find('all', array('order' => "nomeProfessor"));
		$this->set(compact('professor'));
	}

	public function add()    {
		$this->autoRender = false;
		$this->response->type('json');
		
		if ($this->request->is('post')) {

			$this->loadModel('Usuario');				
			$conn = ConnectionManager::get('default');
			$conn->begin();

			$usuario = $this->Usuario->newEntity([
				'login' => $this->request->getData()['emailProfessor'],
				'password' => (new DefaultPasswordHasher())->hash($this->request->getData()['senhaProfessor']),
				'tipo' => 2,
			]);
			if($this->Usuario->save($usuario)){
				$professor = $this->Professor->newEntity();
				$professor = $this->Professor->patchEntity($professor, $this->request->getData());
				$professor->usuario_idUsuario = $usuario->idUsuario;
				if ($this->Professor->save($professor)) {
					$this->response->statusCode(200);
					$this->response->body(json_encode(array('result' => 'success','id'=>$professor->idProfessor, 'nome'=>$professor->nomeProfessor,
						'email'=>$professor->emailProfessor,'telefone'=>$professor->telefoneProfessor)));
					$conn->commit();
				}else{
					$this->response->statusCode(200);
					$this->response->body(json_encode(array('result' => 'error')));
					$conn->rollback();				
				}
			}else{
				$this->response->statusCode(200);
				$this->response->body(json_encode(array('result' => 'error')));
			}
		}
		return $this->response;
	}
	public function delete()    {
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idProfessor = $this->request->data['idProfessor'];
		try {
			$professor = $this->Professor->get($idProfessor);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		
		if($this->Professor->delete($professor)){			
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'success')));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}
	public function update(){
		$this->autoRender = false;
		$this->response->type('json');
		$this->request->allowMethod(['post']);
		$idProfessor = $this->request->data['idProfessor'];
		try {
			$professor = $this->Professor->get($idProfessor);	
		} catch (Exception $e) {
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
			return $this->response;
		}
		$professor->nomeProfessor = $this->request->data['nomeProfessor'];
		$professor->emailProfessor = $this->request->data['emailProfessor'];
		$professor->telefoneProfessor = $this->request->data['telefoneProfessor'];
		if($this->Professor->save($professor)){			
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'success','id'=>$professor->idProfessor, 'nome'=>$professor->nomeProfessor,
				'email'=>$professor->emailProfessor,'telefone'=>$professor->telefoneProfessor)));
		}else{
			$this->response->statusCode(200);
			$this->response->body(json_encode(array('result' => 'error')));
		}
		return $this->response;
	}
	
}

?>