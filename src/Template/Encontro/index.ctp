<div class="col s10 offset-s1">
	<div class="divider"></div>
	<div class="col s12 center">
		<h4>Encontros cadastrados</h4>
		<h5>Projeto: <?= $projeto->tituloProjeto ?></h5>
	</div>
	<div class="col s12 section " style="padding-top: 25px" >		
		<div class="col s7">			
			<ul>		
				<a  onClick="$('#modal_encontro_create').modal('open')" class="btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Cadastrar nova encontro"><i class="material-icons">add</i></a>		
				<a id="bt_edit_encontro" disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Editar encontro selecionada"><i class="material-icons" >mode_edit</i></a>

				<a id="bt_del_encontro"  disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Excluir encontro selecionada"><i class="material-icons" >delete</i></a>	
				<a id="bt_detail_encontro"  disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Detalhar encontro selecionada"><i class="material-icons" >remove_red_eye</i></a>		
			</ul>	
		</div>
		<div class="col s4 offset">			
			<div class="input-field" >
				<input id="field_search_encontro" type="text" class="validate">
				<label for="search">Procurar</label>
			</div>
		</div>
		<div class="col s1">			
			<ul class="">
				<a id="bt_search_encontro" class="btn-floating btn-large waves-effect waves-light red tooltipped"  data-position="bottom" data-delay="150" data-tooltip="Procurar encontro"><i class="material-icons">search</i></a>	
			</ul>
		</div>
	</div>		
	<table id="table_encontro" class="bordered responsive-table">
		<thead>
			<tr>
				<th>Tema</th>
				<th>Data</th>
				<th>Hora</th>				
				<th>Local</th>				
			</tr>
		</thead>
		<tbody>
			<?php foreach ($encontro as $a): ?>
				<tr >
					<td name="id" hidden><?=  $a->idEncontro ?></td>
					<td ><?= $a->temaEncontro ?></td>
					<td><?= $a->data ?></td>
					<td><?= $a->hora->format('h:m') ?></td>
					<td><?= $a->local ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<ul id="pagination_encontro" class="col s12 pagination center">
	</ul>
</div>
<div id="modal_encontro_edit" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Editar encontro</h4>
					</div>
					<form id="form_edit_encontro" action="encontro/add" method="post">
						<input hidden name="idEncontro"  type="text" >
							<div class="col s12 input-field">
							<i class="material-icons prefix">person</i>
							<input name="temaEncontro" type="text" class="validate">
							<label>Tema</label>
						</div>	
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="descEncontro" id="textarea1" class="materialize-textarea"></textarea>
							<label>Descrição</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="atividades" id="textarea1" class="materialize-textarea"></textarea>
							<label>Atividades</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="problemas" id="textarea1" class="materialize-textarea"></textarea>
							<label>Problemas</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="atividadesFuturas" id="textarea1" class="materialize-textarea"></textarea>
							<label>Atividades futuras</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input name="data" type="text" class="datepicker">
							<label>Data</label>
						</div>
						
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input name="hora" type="text" class="timepicker">
							<label>Hora</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">person</i>
							<input name="local" type="text" class="validate">
							<label>Local</label>
						</div>	
						
						<div class="row">
							<button style="margin-right: 20px" class=" col s4 offset-s2 btn waves-effect waves-light center modal-action" type="submit" name="save">Salvar
								<i class="material-icons right">send</i>
							</button>
							<button onclick="event.preventDefault()" class="btn red col s4   waves-effect waves-light center modal-close">Cancelar</button>
						</div>	
					</form>			
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<div id="modal_encontro_create" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Cadastrar encontro</h4>
					</div>
					<form id="form_cad_encontro" method="post">
						<div class="col s12 input-field">
							<i class="material-icons prefix">person</i>
							<input name="temaEncontro" type="text" class="validate">
							<label>Tema</label>
						</div>	
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="descEncontro" id="textarea1" class="materialize-textarea"></textarea>
							<label>Descrição</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="atividades" id="textarea1" class="materialize-textarea"></textarea>
							<label>Atividades</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="problemas" id="textarea1" class="materialize-textarea"></textarea>
							<label>Problemas</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="atividadesFuturas" id="textarea1" class="materialize-textarea"></textarea>
							<label>Atividades futuras</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input name="data" type="text" class="datepicker">
							<label>Data</label>
						</div>
						
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input name="hora" type="text" class="timepicker">
							<label>Hora</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">person</i>
							<input name="local" type="text" class="validate">
							<label>Local</label>
						</div>	
						
						
						<div class="row">
							<button style="margin-right: 20px" class="col s4 offset-s2 btn waves-effect waves-light center modal-action" type="submit" name="save">Cadastrar
								<i class="material-icons right">send</i>
							</button>
							<button onclick="event.preventDefault()" class="btn red col s4 waves-effect waves-light center modal-close">Cancelar</button>
						</div>	
					</form>			
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<div class="center-modal">	
	<div id="modal_encontro_delete" class="modal">
		<div class="modal-content">
			<div class="col s12">	
				<div class="card">			
					<div class="row card-content">	
						<div class="col s12 center">
							<h4>Excluir encontro</h4>
						</div>
						<div class="center">
							<h5>Excluir encontro(a) <span id="del_encontro_name"></span>?</h5>
						</div>
						<div class="row center">
							<button id="bt_del_encontro_confirm" style="margin-right: 20px" class="col s4 offset-s2 btn waves-effect waves-light center modal-action red modal-close">Excluir
							</button>
							<button style="margin-right: 20px" class="col s4 offset btn waves-effect waves-light center modal-action modal-close">Cancelar
							</button>
						</div>
					</div>		
				</div>	
			</div>
		</div>
		<div class="modal-footer">			
		</div>
	</div>
</div>
<div id="modal_encontro_detail" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Detalhar encontro</h4>
					</div>
					<form id="form_detail_encontro">
						<div class="col s12 input-field">
							<i class="material-icons prefix">title</i>
							<input readonly name="temaEncontro" id="icon_prefix" type="text" >
							<label>Tema Encontro</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea readonly name="descEncontro" id="textarea1" class="materialize-textarea"></textarea>
							<label>Descrição</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea readonly name="atividades" id="textarea1" class="materialize-textarea"></textarea>
							<label>Atividades</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea readonly name="problemas" id="textarea1" class="materialize-textarea"></textarea>
							<label>Problemas</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea readonly name="atividadesFuturas" id="textarea1" class="materialize-textarea"></textarea>
							<label>Atividades Futuras</label>
						</div>

						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input readonly name="data" type="text">
							<label>Data</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input readonly name="hora" type="text">
							<label>Hora</label>
						</div>
						<div  class="col s12  input-field">
							<i class="material-icons prefix">lock</i>
							<input readonly name="local" type="text">
							<label>Local</label>
						</div>
		
					</form>
					<div class="row">
						<button onclick="event.preventDefault()" class="btn red col s4 offset-s4  waves-effect waves-light center modal-close">Fechar</button>
					</div>	
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<?= $this->Html->script('encontro.js'); ?>	