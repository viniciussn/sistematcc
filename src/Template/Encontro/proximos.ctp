
<div class="col s10 offset-s1">
	<div class="divider"></div>
	<div class="col s12 center">
		<h4>Próximos encontros</h4>
	</div>
	<div class="col s12 section " style="padding-top: 25px" >		
		<div  class="col s4  input-field">
			<i class="material-icons prefix">person</i>
			<select name="professor_orientador1">
				<option value="" disabled selected>Projeto</option>
				<?php foreach ($projetos as $a): ?>
					<option value=<?= $a->idProjeto ?>><?= $a->tituloProjeto ?></option>
				<?php endforeach; ?> 	
			</select>
		</div>
		<div class="col s4">	
			<div  class="col s12  input-field">
				<i class="material-icons prefix">person</i>
				<select name="periodo">
					
					<option value="7">Próximos 7 dias</option>
					<option value="15">Próximos 15 dias</option>
					<option value="30">Próximos 30 dias</option>

				</select>
				<label for="periodo">Período</label>
			</div>					
		</div>	
		<div class="col s3 input-field">
			<input id="field_search_encontro" type="text" class="validate">
			<label for="search">Procurar...</label>
		</div>	
		<div class="col s1">
			<ul class="">
				<a id="bt_search_encontro" class="btn-floating btn-large waves-effect waves-light red tooltipped"  data-position="bottom" data-delay="150" data-tooltip="Procurar encontro"><i class="material-icons">search</i></a>	
			</ul>
		</div>	
	</div>		

	<table id="table_encontro" class="bordered responsive-table">
		<thead>
			<tr>
				<th>Tema</th>
				<th>Projeto</th>
				<th>Data</th>
				<th>Hora</th>				
				<th>Local</th>				
			</tr>
		</thead>
		<tbody>
			<?php foreach ($encontros as $a): ?>
				<tr >
					<td name="id" hidden><?=  $a->idEncontro ?></td>
					<td ><?= $a['temaEncontro'] ?></td>
					<td ><?= $a['tituloProjeto'] ?></td>
					<td><?= $a['data'] ?></td>
					<td><?= $a['hora'] ?></td>
					<td><?= $a['local'] ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<ul id="pagination_encontro" class="col s12 pagination center">
	</ul>
</div>
<?= $this->Html->script('encontro_proximos.js'); ?>	