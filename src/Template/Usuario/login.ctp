<div class="row">
	<div class="col s4 offset-s4">			
		<div class="card">			
			<div class="row card-content">	
				<form method='post' action='/usuario/login'>
					<div class="col s12 offset input-field">
						<i class="material-icons prefix">email</i>
						<input name="login" type="text" class="validate">
						<label for="icon_prefix">Email</label>
					</div>

					<div class="col s12 offset input-field">
						<i class="material-icons prefix">lock</i>
						<input name="password" type="password" class="validate">
						<label for="icon_telephone">Senha</label>
					</div>
					<div class="row">			
						<button class="col s6 offset-s3 btn waves-effect waves-light" type="submit" name="action">Login
							<i class="material-icons right">send</i>
						</button>
					</div>				
				</form>
			</div>
		</div>
	</div>
</div>
