<div class="col s10 offset-s1">
	<div class="divider"></div>
	<div class="col s12 center">
		<h4>Projetos cadastrados</h4>
	</div>
	<div class="col s12 section " style="padding-top: 25px" >		
		<div class="col s7">			
			<ul>		
				<a  onClick="$('#modal_projeto_create').modal('open')" class="btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Cadastrar novo projeto"><i class="material-icons">add</i></a>		
				<a id="bt_edit_projeto" disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Editar projeto selecionado"><i class="material-icons" >mode_edit</i></a>

				<a id="bt_del_projeto"  disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Excluir projeto selecionado"><i class="material-icons" >delete</i></a>	
				<a id="bt_detail_projeto"  disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Detalhar projeto selecionado"><i class="material-icons" >remove_red_eye</i></a>		
			</ul>	
		</div>
		<div class="col s4 offset">			
			<div class="input-field" >
				<input id="field_search_projeto" type="text" class="validate">
				<label for="search">Procurar</label>
			</div>
		</div>
		<div class="col s1">			
			<ul class="">
				<a id="bt_search_projeto" class="btn-floating btn-large waves-effect waves-light red tooltipped"  data-position="bottom" data-delay="150" data-tooltip="Procurar projeto"><i class="material-icons">search</i></a>	
			</ul>
		</div>
	</div>		
	<table id="table_projeto" class="bordered responsive-table">
		<thead>
			<tr>
				<th>Titulo</th>
				<th>Professor orientador</th>
				<th>Aluno</th>				
				<th>Status</th>
				<th></th>	
				<th></th>				
			</tr>
		</thead>
		<tbody>
			<?php foreach ($projetos as $a): ?>
				<tr >
					<td name="id" hidden><?=  $a->idProjeto ?></td>
					<td ><?=  $a->tituloProjeto ?></td>
					<td><?= $a->professor1->nomeProfessor ?></td>
					<td><?= $a->aluno->nomeAluno ?></td>
					<td><?= $a->status->nomeStatus ?></td>
					<td><a class=" waves-effect waves-light btn-small" href="/etapa/mostrar/<?=  $a->idProjeto ?>"><i class="material-icons left">open_in_new</i>Etapas</a></td>
					<td><a class=" waves-effect waves-light btn-small" href="/encontro/mostrar/<?=  $a->idProjeto ?>"><i class="material-icons left">open_in_new</i>Encontros</a></td>
				</tr>

			<?php endforeach; ?>

		</tbody>
	</table>
	<ul id="pagination_projeto" class="pagination center">
	</ul>
</div>
<div id="modal_projeto_edit" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Editar Projeto</h4>
					</div>
					<form id="form_edit_projeto" action="projeto/add" method="post">
						<input hidden name="idProjeto"  type="text" >
						<div class="col s6">							
							<div class="col s12 input-field">
								<i class="material-icons prefix">title</i>
								<input name="tituloProjeto" id="icon_prefix" type="text" class="validate">
								<label for="icon_prefix">Titulo</label>
							</div>
							<div class="col s12 input-field">
								<i class="material-icons prefix">description</i>
								<textarea name="descProjeto" id="textarea1" class="materialize-textarea"></textarea>
								<label for="icon_telephone">Descrição</label>
							</div>
							<div class="col s12 input-field">
								<i class="material-icons prefix">date_range</i>
								<input name="dtInicio" type="text" class="datepicker">
								<label for="icon_telephone">Data de início</label>
							</div>
							<div class="col s12 input-field">
								<i class="material-icons prefix">date_range</i>
								<input name="dtFim" type="text" class="datepicker">
								<label for="icon_telephone">Data de fim</label>
							</div>
							<div  class="col s12  input-field">
								<i class="material-icons prefix">lock</i>
								<select name="status_idstatus">
									<option value="" disabled selected>Status</option>
									<?php foreach ($status as $a): ?>
										<option value= <?= $a->idStatus ?>  > <?= $a->nomeStatus ?></option>
									<?php endforeach; ?>	
								</select>
							</div>
						</div>

						<div class="col s6">
							<div  class="col s12  input-field">
								<i class="material-icons prefix">person</i>
								<select name="professor_orientador1">
									<option value="" disabled selected>Orientador</option>
									<?php foreach ($professores as $a): ?>
										<option value=<?= $a->idProfessor ?>><?= $a->nomeProfessor ?></option>
									<?php endforeach; ?>	
								</select>
							</div>
							<div  class="col s12  input-field">
								<i class="material-icons prefix">person</i>
								<select name="professor_orientador2">
									<option value="" disabled selected>Co-Orientador</option>
									<?php foreach ($professores as $a): ?>
										<option value=<?= $a->idProfessor ?>><?= $a->nomeProfessor ?></option>
									<?php endforeach; ?>	
								</select>
							</div>
							<div  class="col s12  input-field">
								<i class="material-icons prefix">person</i>
								<select name="aluno_idaluno">
									<option value="" disabled selected>Aluno</option>
									<?php foreach ($alunos as $a): ?>
										<option value=<?= $a->idAluno ?>><?= $a->nomeAluno ?></option>
									<?php endforeach; ?>								
								</select>
							</div>
						</div>
						<div class="row">
							<button style="margin-right: 20px" class=" col s4 offset-s2 btn waves-effect waves-light center modal-action" type="submit" name="save">Salvar
								<i class="material-icons right">send</i>
							</button>
							<button onclick="event.preventDefault()" class="btn red col s4   waves-effect waves-light center modal-close">Cancelar</button>
						</div>	
					</form>			
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<div id="modal_projeto_create" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Cadastrar Projeto</h4>
					</div>
					<form id="form_cad_projeto" method="post">
						<div class="col s6">							
							<div class="col s12 input-field">
								<i class="material-icons prefix">title</i>
								<input name="tituloProjeto" id="icon_prefix" type="text" class="validate">
								<label for="icon_prefix">Titulo</label>
							</div>
							<div class="col s12 input-field">
								<i class="material-icons prefix">description</i>
								<textarea name="descProjeto" id="textarea1" class="materialize-textarea"></textarea>
								<label for="icon_telephone">Descrição</label>
							</div>
							<div class="col s12 input-field">
								<i class="material-icons prefix">date_range</i>
								<input name="dtInicio" type="text" class="datepicker">
								<label for="icon_telephone">Data de início</label>
							</div>
							<div class="col s12 input-field">
								<i class="material-icons prefix">date_range</i>
								<input name="dtFim" type="text" class="datepicker">
								<label for="icon_telephone">Data de fim</label>
							</div>
							<div  class="col s12  input-field">
								<i class="material-icons prefix">lock</i>
								<select name="status_idstatus">
									<option value="" disabled selected>Status</option>
									<?php foreach ($status as $a): ?>
										<option value=<?= $a->idStatus ?>><?= $a->nomeStatus ?></option>
									<?php endforeach; ?>	
								</select>
							</div>
						</div>
						<div class="col s6">
							<div  class="col s12  input-field">
								<i class="material-icons prefix">person</i>
								<select name="professor_orientador1">
									<option value="" disabled selected>Orientador</option>
									<?php foreach ($professores as $a): ?>
										<option value=<?= $a->idProfessor ?>><?= $a->nomeProfessor ?></option>
									<?php endforeach; ?>	
								</select>
							</div>
							<div  class="col s12  input-field">
								<i class="material-icons prefix">person</i>
								<select name="professor_orientador2">
									<option value="" disabled selected>Co-Orientador</option>
									<?php foreach ($professores as $a): ?>
										<option value=<?= $a->idProfessor ?>><?= $a->nomeProfessor ?></option>
									<?php endforeach; ?>	
								</select>
							</div>
							<div  class="col s12  input-field">
								<i class="material-icons prefix">person</i>
								<select name="aluno_idaluno">
									<option value="" disabled selected>Aluno</option>
									<?php foreach ($alunos as $a): ?>
										<option value=<?= $a->idAluno ?>><?= $a->nomeAluno ?></option>
									<?php endforeach; ?>								
								</select>
							</div>
						</div>
						<div class="row">
							<button style="margin-right: 20px" class="col s4 offset-s2 btn waves-effect waves-light center modal-action" type="submit" name="save">Cadastrar
								<i class="material-icons right">send</i>
							</button>
							<button onclick="event.preventDefault()" class="btn red col s4 waves-effect waves-light center modal-close">Cancelar</button>
						</div>	
					</form>			
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<div class="center-modal">	
	<div id="modal_projeto_delete" class="modal">
		<div class="modal-content">
			<div class="col s12">	
				<div class="card">			
					<div class="row card-content">	
						<div class="col s12 center">
							<h4>Excluir Projeto</h4>
						</div>
						<div class="center">
							<h5>Excluir projeto(a) <span id="del_projeto_name"></span>?</h5>
						</div>
						<div class="row center">
							<button id="bt_del_projeto_confirm" style="margin-right: 20px" class="col s4 offset-s2 btn waves-effect waves-light center modal-action red modal-close">Excluir
							</button>
							<button style="margin-right: 20px" class="col s4 offset btn waves-effect waves-light center modal-action modal-close">Cancelar
							</button>
						</div>
					</div>		
				</div>	
			</div>
		</div>
		<div class="modal-footer">			
		</div>
	</div>
</div>
<div id="modal_projeto_detail" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Detalhar Projeto</h4>
					</div>
					<form id="form_detail_projeto">
						<input hidden name="idProjeto"  type="text" >
						<div class="col s6">							
							<div class="col s12 input-field">
								<i class="material-icons prefix">title</i>
								<input readonly name="tituloProjeto" id="icon_prefix" type="text" >
								<label>Titulo</label>
							</div>
							<div class="col s12 input-field">
								<i class="material-icons prefix">description</i>
								<textarea readonly name="descProjeto" id="textarea1" class="materialize-textarea"></textarea>
								<label>Descrição</label>
							</div>
							<div class="col s12 input-field">
								<i class="material-icons prefix">date_range</i>
								<input readonly name="dtInicio" type="text">
								<label>Data de início</label>
							</div>
							<div class="col s12 input-field">
								<i class="material-icons prefix">date_range</i>
								<input readonly name="dtFim" type="text">
								<label>Data de fim</label>
							</div>
							<div  class="col s12  input-field">
								<i class="material-icons prefix">lock</i>
								<input readonly name="status_idstatus" type="text">
								<label>Status</label>
							</div>
						</div>
						<div class="col s6">
							<div  class="col s12  input-field">
								<i class="material-icons prefix">person</i>
								<input readonly name="professor_orientador1" id="icon_prefix" type="text" >
								<label>Professor Orientador</label>
							</div>
							<div  class="col s12  input-field">
								<i class="material-icons prefix">person</i>
								<input readonly name="professor_orientador2" id="icon_prefix" type="text" >
								<label>Professor Co-orientador</label>
							</div>
							<div  class="col s12  input-field">
								<i class="material-icons prefix">person</i>
								<input readonly name="aluno_idaluno" id="icon_prefix" type="text" >
								<label>Aluno</label>
							</div>
						</div>
					</form>
					<div class="row">
						<button onclick="event.preventDefault()" class="btn red col s4 offset-s4  waves-effect waves-light center modal-close">Fechar</button>
					</div>	
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<?= $this->Html->script('projeto.js'); ?>	