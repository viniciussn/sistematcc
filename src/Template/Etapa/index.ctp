<div class="col s10 offset-s1">
	<div class="divider"></div>
	<div class="col s12 center">
		<h4>Etapas cadastradas</h4>
		<h5>Projeto: <?= $projeto->tituloProjeto ?></h5>
	</div>
	<div class="col s12 section " style="padding-top: 25px" >		
		<div class="col s7">			
			<ul>		
				<a  onClick="$('#modal_etapa_create').modal('open')" class="btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Cadastrar nova etapa"><i class="material-icons">add</i></a>		
				<a id="bt_edit_etapa" disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Editar etapa selecionada"><i class="material-icons" >mode_edit</i></a>

				<a id="bt_del_etapa"  disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Excluir etapa selecionada"><i class="material-icons" >delete</i></a>	
				<a id="bt_detail_etapa"  disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Detalhar etapa selecionada"><i class="material-icons" >remove_red_eye</i></a>		
			</ul>	
		</div>
		<div class="col s4 offset">			
			<div class="input-field" >
				<input id="field_search_etapa" type="text" class="validate">
				<label for="search">Procurar</label>
			</div>
		</div>
		<div class="col s1">			
			<ul class="">
				<a id="bt_search_etapa" class="btn-floating btn-large waves-effect waves-light red tooltipped"  data-position="bottom" data-delay="150" data-tooltip="Procurar etapa"><i class="material-icons">search</i></a>	
			</ul>
		</div>
	</div>		
	<table id="table_etapa" class="bordered responsive-table">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Data de início</th>
				<th>Status</th>				
			</tr>
		</thead>
		<tbody>
			<?php foreach ($etapa as $a): ?>
				<tr >
					<td name="id" hidden><?=  $a->idEtapa ?></td>
					<td ><?= $a->nomeEtapa ?></td>
					<td><?= $a->dtInicio ?></td>
					<td><?= $a->status_etapa->nomeStatus ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<ul id="pagination_etapa" class="col s12 pagination center">
	</ul>
</div>
<div id="modal_etapa_edit" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Editar etapa</h4>
					</div>
					<form id="form_edit_etapa" action="etapa/add" method="post">
						<input hidden name="idEtapa"  type="text" >
						<div class="col s12 input-field">
							<i class="material-icons prefix">person</i>
							<input name="nomeEtapa" type="text" class="validate">
							<label  for="icon_prefix">Nome</label>
						</div>	
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="descEtapa" id="textarea1" class="materialize-textarea"></textarea>
							<label for="icon_telephone">Descrição</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input name="dtInicio" type="text" class="datepicker">
							<label for="icon_telephone">Data de início</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input name="dtFim" type="text" class="datepicker">
							<label for="icon_telephone">Data de fim</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="comentarioEtapa" id="textarea1" class="materialize-textarea"></textarea>
							<label for="icon_telephone">Comentários</label>
						</div>
						<div  class="col s12  input-field">
							<i class="material-icons prefix">lock</i>
							<select name="statusEtapa_idStatus">
								<?php foreach ($status as $a): ?>
									<option value= <?= $a->idStatus ?>  > <?= $a->nomeStatus ?></option>
								<?php endforeach; ?>	
							</select>
							<label>Status</label>
						</div>
						<div class="row">
							<button style="margin-right: 20px" class=" col s4 offset-s2 btn waves-effect waves-light center modal-action" type="submit" name="save">Salvar
								<i class="material-icons right">send</i>
							</button>
							<button onclick="event.preventDefault()" class="btn red col s4   waves-effect waves-light center modal-close">Cancelar</button>
						</div>	
					</form>			
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<div id="modal_etapa_create" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Cadastrar etapa</h4>
					</div>
					<form id="form_cad_etapa" method="post">
						<div class="col s12 input-field">
							<i class="material-icons prefix">person</i>
							<input name="nomeEtapa" type="text" class="validate">
							<label  for="icon_prefix">Nome</label>
						</div>	
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="descEtapa" id="textarea1" class="materialize-textarea"></textarea>
							<label for="icon_telephone">Descrição</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input name="dtInicio" type="text" class="datepicker">
							<label for="icon_telephone">Data de início</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input name="dtFim" type="text" class="datepicker">
							<label for="icon_telephone">Data de fim</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea name="comentarioEtapa" id="textarea1" class="materialize-textarea"></textarea>
							<label for="icon_telephone">Comentários</label>
						</div>
						<div  class="col s12  input-field">
							<i class="material-icons prefix">lock</i>
							<select name="statusEtapa_idStatus">
								<?php foreach ($status as $a): ?>
									<option value= <?= $a->idStatus ?>  > <?= $a->nomeStatus ?></option>
								<?php endforeach; ?>	
							</select>
							<label>Status</label>
						</div>
						<div class="row">
							<button style="margin-right: 20px" class="col s4 offset-s2 btn waves-effect waves-light center modal-action" type="submit" name="save">Cadastrar
								<i class="material-icons right">send</i>
							</button>
							<button onclick="event.preventDefault()" class="btn red col s4 waves-effect waves-light center modal-close">Cancelar</button>
						</div>	
					</form>			
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<div class="center-modal">	
	<div id="modal_etapa_delete" class="modal">
		<div class="modal-content">
			<div class="col s12">	
				<div class="card">			
					<div class="row card-content">	
						<div class="col s12 center">
							<h4>Excluir etapa</h4>
						</div>
						<div class="center">
							<h5>Excluir etapa(a) <span id="del_etapa_name"></span>?</h5>
						</div>
						<div class="row center">
							<button id="bt_del_etapa_confirm" style="margin-right: 20px" class="col s4 offset-s2 btn waves-effect waves-light center modal-action red modal-close">Excluir
							</button>
							<button style="margin-right: 20px" class="col s4 offset btn waves-effect waves-light center modal-action modal-close">Cancelar
							</button>
						</div>
					</div>		
				</div>	
			</div>
		</div>
		<div class="modal-footer">			
		</div>
	</div>
</div>
<div id="modal_etapa_detail" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Detalhar etapa</h4>
					</div>
					<form id="form_detail_etapa">
						<div class="col s12 input-field">
							<i class="material-icons prefix">title</i>
							<input readonly name="nomeEtapa" id="icon_prefix" type="text" >
							<label>Nome Etapa</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea readonly name="descEtapa" id="textarea1" class="materialize-textarea"></textarea>
							<label>Descrição</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input readonly name="dtInicio" type="text">
							<label>Data de início</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">date_range</i>
							<input readonly name="dtFim" type="text">
							<label>Data de fim</label>
						</div>
						<div  class="col s12  input-field">
							<i class="material-icons prefix">lock</i>
							<input readonly name="status" type="text">
							<label>Status</label>
						</div>
						<div  class="col s12  input-field">
							<i class="material-icons prefix">lock</i>
							<input readonly name="projeto" type="text">
							<label>Projeto</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">description</i>
							<textarea readonly name="comentarioEtapa" id="textarea1" class="materialize-textarea"></textarea>
							<label>Comentário</label>
						</div>
					</form>
					<div class="row">
						<button onclick="event.preventDefault()" class="btn red col s4 offset-s4  waves-effect waves-light center modal-close">Fechar</button>
					</div>	
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<?= $this->Html->script('etapa.js'); ?>	