<!DOCTYPE html>
<html>
<head>
	<title>Sistema TCC</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
	<?= $this->Html->css('materialize.min.css'); ?>
	<?= $this->Html->css('custom.css'); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<?= $this->Html->script('util.js'); ?>	
<?= $this->Html->script('materialize.min.js'); ?>
<?= $this->Html->script('custom.js'); ?>	
<body class="blue-grey lighten-4">	
	<div class="white">	
		<nav>
			<div class="nav-wrapper teal lighten-2" >
				<div class="container">
					
					<a href="/" class="brand-logo">Sistema TCC</a>
					<ul  class="right hide-on-med-and-down">

						<?php 
						$session = $this->request->session();
						if($session->read('Auth.User')){							
							if($session->read('user.tipo')==3){
								echo'
								<li><a href="/encontro/proximos">Próximos encontros</a></li>	
								<li><a href="/projeto">Projeto</a></li>					
								<li><a href="/usuario/logout">Sair</a></li>';
							}else if($session->read('user.tipo')==2){
								echo'
								<li><a href="/encontro/proximos">Próximos encontros</a></li>
								<li><a href="/projeto">Projeto</a></li>	
								<li><a href="/usuario/logout">Sair</a></li>';
							}
							else if($session->read('user.tipo')==1){
								echo'
								<li><a href="/encontro/proximos">Próximos encontros</a></li>
								<li><a href="/projeto">Projeto</a></li>	
								<li><a href="/professor">Professor</a></li>	
								<li><a href="/aluno">Aluno</a></li>		
								<li><a href="/usuario/logout">Sair</a></li>';
							}
						}else{
							echo '<li><a href="/usuario/login">Login</a></li>';
						}
						?>

					</ul>
					<ul id="nav-mobile" class="side-nav">
						<li><a href="#">Navbar Link</a></li>
					</ul>
					<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
				</div>
			</div>
		</nav>
		<div id="content" class="row section">
			<?= $this->fetch('content') ?>
		</div><footer class="page-footer teal lighten-2">
			<!--
			
			<div class="container">
				<div class="row">
					<div class="col l6 s12">
						<h5 class="white-text">Footer Content</h5>
						<p class="grey-text text-lighten-4">Conteúdo do rodapé.</p>
					</div>
					<div class="col l4 offset-l2 s12">
						<h5 class="white-text">Links</h5>
						<ul>
							<li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
							<li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
							<li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
							<li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
						</ul>
					</div>
				</div>
			</div>
		-->
		<div class="footer-copyright">
			<div class="container">
				© 2017 Copyright Text
				<a class="grey-text text-lighten-4 right" href="#!"></a>
			</div>
		</div>
	</footer>
</div>



</body>
</html>