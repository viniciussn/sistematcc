<div class="col s10 offset-s1">
	<div class="divider"></div>
	<div class="col s12 center">
		<h4>Professores cadastrados</h4>
	</div>
	<div class="col s12 section " style="padding-top: 25px" >		
		<div class="col s7">			
			<ul>		
				<a  onClick="$('#modal_professor_create').modal('open')" class="btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Cadastrar novo professor"><i class="material-icons">add</i></a>		
				<a id="bt_edit_professor" disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Editar professor selecionado"><i class="material-icons" >mode_edit</i></a>

				<a id="bt_del_professor"  disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Excluir professor selecionado"><i class="material-icons" >delete</i></a>	
				<a id="bt_detail_professor"  disabled class=" btn-floating btn-large waves-effect waves-light red tooltipped" data-position="bottom" data-delay="150" data-tooltip="Detalhar professor selecionado"><i class="material-icons" >remove_red_eye</i></a>		
			</ul>	
		</div>
		<div class="col s4 offset">			
			<div class="input-field" >
				<input id="field_search_professor" type="text" class="validate">
				<label for="search">Procurar</label>
			</div>
		</div>
		<div class="col s1">			
			<ul class="">
				<a id="bt_search_professor" class="btn-floating btn-large waves-effect waves-light red tooltipped"  data-position="bottom" data-delay="150" data-tooltip="Procurar professor"><i class="material-icons">search</i></a>	
			</ul>
		</div>
	</div>		
	<table id="table_professor" class="bordered responsive-table">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Email</th>
				<th>Telefone</th>				
			</tr>
		</thead>
		<tbody>
			<?php foreach ($professor as $a): ?>
				<tr >
					<td name="id" hidden><?=  $a->idProfessor ?></td>
					<td ><?=  $a->nomeProfessor ?></td>
					<td><?= $a->emailProfessor ?></td>
					<td><?= $a->telefoneProfessor ?></td>
				</tr>

			<?php endforeach; ?>

		</tbody>
	</table>
	<ul id="pagination_professor" class="pagination center">
	</ul>
</div>
<div id="modal_professor_edit" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Editar Professor</h4>
					</div>
					<form id="form_edit_professor" action="professor/add" method="post">
						<input type="hidden" name="idProfessor">
						<div class="col s12 input-field">
							<i class="material-icons prefix">person</i>
							<input name="nomeProfessor" type="text" class="validate">
							<label  for="icon_prefix">Nome</label>
						</div>	
						<div class="col s12 input-field">
							<i class="material-icons prefix">email</i>
							<input name="emailProfessor" type="text" class="validate">
							<label for="icon_prefix">Email</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">phone</i>
							<input name="telefoneProfessor" type="tel" class="validate">
							<label for="icon_telephone">Telefone</label>
						</div>
						<div class="row">
							<button style="margin-right: 20px" class=" col s4 offset-s2 btn waves-effect waves-light center modal-action" type="submit" name="save">Salvar
								<i class="material-icons right">send</i>
							</button>
							<button onclick="event.preventDefault()" class="btn red col s4   waves-effect waves-light center modal-close">Cancelar</button>
						</div>	
					</form>			
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<div id="modal_professor_create" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Cadastrar Professor</h4>
					</div>
					<form id="form_cad_professor" method="post">
						<div class="col s12 input-field">
							<i class="material-icons prefix">person</i>
							<input name="nomeProfessor" type="text" class="validate">
							<label  for="icon_prefix">Nome</label>
						</div>	
						<div class="col s12 input-field">
							<i class="material-icons prefix">email</i>
							<input name="emailProfessor" type="text" class="validate">
							<label for="icon_prefix">Email</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">phone</i>
							<input name="telefoneProfessor" type="tel" class="validate">
							<label for="icon_telephone">Telefone</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">lock</i>
							<input name="senhaProfessor" type="password" class="validate">
							<label for="icon_telephone">Senha</label>
						</div>

						<div class="row">
							<button style="margin-right: 20px" class="col s4 offset-s2 btn waves-effect waves-light center modal-action" type="submit" name="save">Cadastrar
								<i class="material-icons right">send</i>
							</button>
							<button onclick="event.preventDefault()" class="btn red col s4 waves-effect waves-light center modal-close">Cancelar</button>
						</div>	
					</form>			
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
<div class="center-modal">	
	<div id="modal_professor_delete" class="modal">
		<div class="modal-content">
			<div class="col s12">	
				<div class="card">			
					<div class="row card-content">	
						<div class="col s12 center">
							<h4>Excluir Professor</h4>
						</div>
						<div class="center">
							<h5>Excluir professor(a) <span id="del_professor_name"></span>?</h5>
						</div>
						<div class="row center">
							<button id="bt_del_professor_confirm" style="margin-right: 20px" class="col s4 offset-s2 btn waves-effect waves-light center modal-action red modal-close">Excluir
							</button>
							<button style="margin-right: 20px" class="col s4 offset btn waves-effect waves-light center modal-action modal-close">Cancelar
							</button>
						</div>
					</div>		
				</div>	
			</div>
		</div>
		<div class="modal-footer">			
		</div>
	</div>
</div>
<div id="modal_professor_detail" class="modal">
	<div class="modal-content">
		<div class="col s12">	
			<div class="card">			
				<div class="row card-content">	
					<div class="col s12 center">
						<h4>Detalhar Professor</h4>
					</div>
					<form id="form_detail_professor">
						<div class="col s12 input-field">
							<i class="material-icons prefix">person</i>
							<input readonly name="nomeProfessor" type="text">
							<label  for="icon_prefix">Nome</label>
						</div>	
						<div class="col s12 input-field">
							<i class="material-icons prefix">email</i>
							<input readonly name="emailProfessor" type="text">
							<label for="icon_prefix">Email</label>
						</div>
						<div class="col s12 input-field">
							<i class="material-icons prefix">phone</i>
							<input readonly name="telefoneProfessor" type="tel">
							<label for="icon_telephone">Telefone</label>
						</div>
					</form>
					<div class="row">
						<button onclick="event.preventDefault()" class="btn red col s4 offset-s4  waves-effect waves-light center modal-close">Fechar</button>
					</div>	
				</div>		
			</div>	
		</div>
	</div>
	<div class="modal-footer">			
	</div>
</div>
  <?= $this->Html->script('professor.js'); ?>	