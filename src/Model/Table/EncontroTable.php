<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class EncontroTable extends Table {

	public function initialize(array $config) {

		$this->addBehavior('Timestamp');
		$this->belongsTo('Projeto')->setForeignKey('projeto_idProjeto');
		
	}

}

?>