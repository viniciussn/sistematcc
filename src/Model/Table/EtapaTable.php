<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class EtapaTable extends Table {

	public function initialize(array $config) {

		$this->addBehavior('Timestamp');
		$this->belongsTo('Projeto')->setForeignKey('projeto_idProjeto');
		$this->belongsTo('StatusEtapa')->setForeignKey('statusEtapa_idStatus');
	}

}

?>