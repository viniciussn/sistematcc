<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class ProjetoTable extends Table {

	public function initialize(array $config) {
		$this->belongsTo('Professor1', [
			'foreignKey' => 'professor_orientador1',
			'className' => 'Professor'
		]);
		$this->belongsTo('Professor2', [
			'foreignKey' => 'professor_orientador2',
			'className' => 'Professor'
		]);
		
		$this->belongsTo('Aluno')->setForeignKey('aluno_idaluno');
		$this->belongsTo('Status')->setForeignKey('status_idstatus');
		$this->addBehavior('Timestamp');
	}

}

?>