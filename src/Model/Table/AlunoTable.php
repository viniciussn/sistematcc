<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class AlunoTable extends Table {

    public function initialize(array $config) {
    	$this->hasOne('Usuario')->setForeignKey('usuario_idUsuario');;
        $this->addBehavior('Timestamp');
    }

}

?>