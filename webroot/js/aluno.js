/*----------------------------------------------------------------------------------
|                                       ALUNO                                      |
----------------------------------------------------------------------------------*/
(function (){
    var request;
    var page =  UTIL.crud_page('aluno');

    /*  CADASTRO    */
    $('#form_cad_aluno').submit(function(){
        event.preventDefault();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find('input, select, button, textarea');
        var serializedData = $form.serialize();
        $inputs.prop('disabled', true);
        request = $.ajax({
            url: 'aluno/add',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){                
                Materialize.toast('Aluno cadastrado com sucesso!', 4000);
                $form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.id+
                    '</td><td>'+response.nome+
                    '</td><td>'+response.email+
                    '</td><td>'+response.telefone+
                    '</td></tr>');//cria a nova linha da tabela        
                
                $('#table_aluno').children('tbody').append(new_row);
                var rows = $('#table_aluno tbody  tr').get(); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toUpperCase();
                    var keyB = $(b).find('td:eq(1)').text().toUpperCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome

                $.each(rows, function(index, row) {
                    $('#table_aluno').children('tbody').append(row);
                });
                page.pagination($('#table_aluno').find('tr').get());
            }else{
                Materialize.toast('Falha ao cadastrar aluno.', 4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
         Materialize.toast('Falha ao cadastrar aluno.', 4000);
     });
        request.always(function () {
            $inputs.prop('disabled', false);
        });
    });

    /*  EDITAR    */
    $('#bt_edit_aluno').click(function(){
        var row = page.getRow_selected();
        var cells = row[0]['cells'];
        var form = $('#form_edit_aluno');
        var inputs_values = {
            'idAluno': cells[0]['innerText'],
            'nomeAluno' : cells[1]['innerText'],
            'emailAluno' : cells[2]['innerText'],
            'telefoneAluno'   :cells [3]['innerText'],
        };
        form.find('input').val(function (index, value) {
            return inputs_values[this.name];
        });
        $('#modal_aluno_edit').modal('open');
        Materialize.updateTextFields();
    });

    $('#form_edit_aluno').submit(function(){
        event.preventDefault();
        var row = page.getRow_selected();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find('input, select, button, textarea');
        var serializedData = $form.serialize();
        $inputs.prop('disabled', true);
        request = $.ajax({
            url: 'aluno/update',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result=='success'){
                Materialize.toast('Aluno modificado com sucesso!',4000);
                $form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.id+
                    '</td><td>'+response.nome+
                    '</td><td>'+response.email+
                    '</td><td>'+response.telefone+
                    '</td></tr>');//cria a nova linha da tabela        
                var table = $('#table_aluno > tbody');//pega a tabela
                table.append(new_row);//adiciona a nova linha na tabela
                table = $('#table_aluno > tbody');
                var rows = $('tr',table); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toLowerCase();
                    var keyB = $(b).find('td:eq(1)').text().toLowerCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome
                rows.each(function (index,row) {
                    table.append(row);
                });//adiciona na tabela ordenadamente
                $('#modal_aluno_edit').modal('close');
                page.disable_buttons();
                page.getRow_selected().removeClass('selected');
                page.setRow_selected(null);
                row.remove();
                page.pagination($('#table_aluno').find('tr').get());
            }else{
                Materialize.toast('Falha ao modificar aluno.',4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao modificar aluno.',4000);
        });
        request.always(function () {
            $inputs.prop('disabled', false);
        });
    });

    /*  DELETE  */
    $('#bt_del_aluno').click(function(){
        $('#del_aluno_name').text($(page.getRow_selected()).find("td:eq(1)").text());
        $('#modal_aluno_delete').modal('open');
    });
    $('#bt_del_aluno_confirm').click(function(){
        var row =  page.getRow_selected();
        var id = row[0]['cells']['id']['innerText'];
        event.preventDefault();
        if (request) {
            request.abort();
        }
        request = $.ajax({
            url: 'aluno/delete',
            type: 'post',
            dataTye: 'json',
            data: {'idAluno': id},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){
                row.remove();
                if(row.get(0) == page.getRow_selected().get(0)){
                    page.setRow_selected(null);
                    page.disable_buttons();
                }
                Materialize.toast('Aluno excluido com sucesso!', 4000);
                page.pagination($('#table_aluno').find('tr').get());
            }else{
               Materialize.toast('Falha ao deletar aluno.', 4000);
           }

       });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao deletar aluno.', 4000);
        });
    });

    /*  PROCURAR    */
    $("#field_search_aluno").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#bt_search_aluno").click();
        }
    });
    $('#bt_search_aluno').click(function(){
        var chave = $('#field_search_aluno').val();
        var rows = $('#table_aluno').find('tr').get();
        var matches = [];
        matches.push($(rows[0]));
        loop:
        for(var i = 1; i < rows.length; i++){
            var cols = $(rows[i]).find('td').get();
            for(var j = 0; j < cols.length; j++){
                if($(cols[j]).text().toLowerCase().indexOf(chave.toLowerCase()) != -1){
                    $(rows[i]).attr('hidden',false);
                    matches.push($(rows[i]));
                    continue loop;
                }
            }
            $(rows[i]).attr('hidden',true);
        }
        page.pagination($(matches));
    });
    /*  DETALHAR    */
    $('#bt_detail_aluno').click(function(){
      var row = page.getRow_selected();
      var cells = row[0]['cells'];
      var form = $('#form_detail_aluno');
      var inputs_values = {
        'nomeAluno' : cells[1]['innerText'],
        'emailAluno' : cells[2]['innerText'],
        'telefoneAluno'   :cells [3]['innerText'],
    };
    form.find('input').val(function (index, value) {
        return inputs_values[this.name];
    });
    $('#modal_aluno_detail').modal('open');
    Materialize.updateTextFields();
});
})();
