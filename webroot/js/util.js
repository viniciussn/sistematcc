/*----------------------------------------------------------------------------------
|                                       UTIL                                       |
----------------------------------------------------------------------------------*/
$(document).ready(function(){
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false, // Close upon selecting a date,
     format: 'yyyy-mm-dd'

});
});



UTIL = (function(){
    function crud_page(name) {
        var row_selected = null; 
        function disable_buttons() {
            $("#bt_del_"+name).attr('disabled', true);
            $("#bt_edit_"+name).attr('disabled', true);
            $("#bt_detail_"+name).attr('disabled', true);
        }
        function enable_buttons() {
         $("#bt_del_"+name).attr('disabled', false);
         $("#bt_edit_"+name).attr('disabled', false);
         $("#bt_detail_"+name).attr('disabled', false);
     }
     $('#table_'+name+' tbody').on('click', 'tr', function() {
        enable_buttons();
        if(row_selected==null){
            $(this).addClass('selected');
            row_selected = $(this);
        }else if(row_selected.get(0) == $(this).get(0)){
            row_selected.removeClass('selected');
            row_selected = null;
            disable_buttons();
        }else{
            row_selected.removeClass('selected');
            $(this).addClass('selected');
            row_selected = $(this);
        }
    });
     function getRow_selected() {
        return row_selected;
    }
    function setRow_selected(row) {
        row_selected = row;
    }
    
    function pagination(rows) {
        $('#pagination_'+name).empty();
        var max = 40;
        var numLinhas = rows.length;
        var numPages = Math.ceil(numLinhas/max);
        var inicio=1;
        var pageAtual = 1;
        var pages = [];
        var arrow_l =   $('<li>').attr('class','disabled pagination-btn').attr('name','l').append(
            $('<a>').attr('href','#!').append(
                $('<i>').attr('class','material-icons').append('chevron_left')
                ));
        if (numPages==1){
               var arrow_r =   $('<li>').attr('class','disabled pagination-btn').attr('name','r').append(
            $('<a>').attr('href','#!').append(
                $('<i>').attr('class','material-icons').append('chevron_right')
                ));

        }else{
               var arrow_r =   $('<li>').attr('class','pagination-btn').attr('name','r').append(
            $('<a>').attr('href','#!').append(
                $('<i>').attr('class','material-icons').append('chevron_right')
                ));
        }
     

        $('#pagination_'+name).append(arrow_l);
        for(var i = 1;i<numPages+1;i++){
            var page =  $('<li>').attr('name',i).attr('class','waves-effect pagination-btn').append(
                $('<a>').attr('href','#!').append(i)                   
                );
            $('#pagination_'+name).append(page);
            pages.push(page);
        }

        $('#pagination_'+name).append(arrow_r);
        pages[0].addClass('active');

        for (var i = 1; i < max+1; i++) {
            $(rows[i]).attr('hidden',false);
        }
        for (var i = max+1; i < numLinhas; i++) {
            $(rows[i]).attr('hidden',true);
        }

        $('.pagination-btn').click(function () {
            var number = $(this).attr('name');
            if(number=='l'){
                number=pageAtual-1;
            }else if(number=='r'){
                number=pageAtual+1;
            }
            if(number<1 || number>numPages) return;   

            pages[pageAtual-1].removeClass('active');
            pages[number-1].addClass('active');
            pageAtual = number;

            for(var i = inicio; i<inicio+max; i++){
                $(rows[i]).attr('hidden',true);
            }
            inicio = (number-1)*max+1;
            for(var i = inicio; i<inicio+max; i++){
                $(rows[i]).attr('hidden',false);
            }

            if(pageAtual==1){
                arrow_l.addClass('disabled');
            }else{
                arrow_l.removeClass('disabled');
            }
            if (pageAtual==numPages){
                arrow_r.addClass('disabled');
            }else{
                arrow_r.removeClass('disabled');
            }
        });
    }
    pagination($('#table_'+name).find('tr').get());
    return {
        'getRow_selected':getRow_selected,
        'setRow_selected':setRow_selected,
        'disable_buttons':disable_buttons,
        'enable_buttons':enable_buttons,
        'pagination':pagination

    }
}
return {'crud_page':crud_page}
})();
