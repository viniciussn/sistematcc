/*----------------------------------------------------------------------------------
|                                       PROJETO                                    |
----------------------------------------------------------------------------------*/
(function (){
    var request;
    var page =  UTIL.crud_page('projeto');

    /*  CADASTRO    */
    $('#form_cad_projeto').submit(function(){
        event.preventDefault();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find('input, select, button, textarea');
        var serializedData = $form.serialize();
        $inputs.prop('disabled', true);
        request = $.ajax({
            url: 'projeto/add',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){                
                Materialize.toast('Projeto cadastrado com sucesso!', 4000);
                $form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.idProjeto+
                    '</td><td>'+response.tituloProjeto+
                    '</td><td>'+response.professor+
                    '</td><td>'+response.aluno+
                    '</td><td>'+response.status+
                    '</td><td>'+'<a class=" waves-effect waves-light btn-small" href="/etapa/mostrar/'+
                    response.idProjeto+'"><i class="material-icons left">open_in_new</i>Etapas</a>'+
                    '</td><td>'+'<a class=" waves-effect waves-light btn-small" href="/encontro/mostrar/'+
                    response.idProjeto+'"><i class="material-icons left">open_in_new</i>Encontros</a>'+
                    '</td></tr>');//cria a nova linha da tabela        
                
                $('#table_projeto').children('tbody').append(new_row);
                var rows = $('#table_projeto tbody  tr').get(); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toUpperCase();
                    var keyB = $(b).find('td:eq(1)').text().toUpperCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome

                $.each(rows, function(index, row) {
                    $('#table_projeto').children('tbody').append(row);
                });
                page.pagination($('#table_projeto').find('tr').get());
            }else{
                Materialize.toast('Falha ao cadastrar projeto.', 4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
         Materialize.toast('Falha ao cadastrar projeto.', 4000);
     });
        request.always(function () {
            $inputs.prop('disabled', false);
        });
    });

    /*  EDITAR    */
    $('#bt_edit_projeto').click(function(){
        var row = page.getRow_selected();
        var cells = row[0]['cells'];        
        request = $.ajax({
            url: 'projeto/find',
            type: 'post',
            data: {'idProjeto': cells[0]['innerText']},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            var form = $('#form_edit_projeto');
            var inputs_values = {
                'idProjeto': response.idProjeto,
                'tituloProjeto' : response.tituloProjeto,
                'descProjeto' : response.descProjeto,
                'dtInicio' : response.dtInicio,
                'dtFim' : response.dtFim,
                'status_idstatus' : response.status_idstatus,
                'aluno_idaluno' : response.aluno_idaluno,
                'professor_orientador1' : response.professor_orientador1,
                'professor_orientador2' : response.professor_orientador2,
            };
            form.find('input, select, button, textarea').val(function (index, value) {
                return inputs_values[this.name];
            });

            $('#modal_projeto_edit').modal('open');
            $('select').material_select();
            Materialize.updateTextFields();

        });
    });
    //salvar
    $('#form_edit_projeto').submit(function(){
        event.preventDefault();
        var row = page.getRow_selected();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find('input, select, button, textarea');
        var serializedData = $form.serialize();
        console.log(serializedData);

        $inputs.prop('disabled', true);
        request = $.ajax({
            url: 'projeto/update',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result=='success'){
                Materialize.toast('Projeto modificado com sucesso!',4000);
                $form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.idProjeto+
                    '</td><td>'+response.tituloProjeto+
                    '</td><td>'+response.professor+
                    '</td><td>'+response.aluno+
                    '</td><td>'+response.status+
                    '</td><td>'+'<a class=" waves-effect waves-light btn-small" href="/etapa/mostrar/'+
                    response.idProjeto+'"><i class="material-icons left">open_in_new</i>Etapas</a>'+
                    '</td><td>'+'<a class=" waves-effect waves-light btn-small" href="/encontro/mostrar/'+
                    response.idProjeto+'"><i class="material-icons left">open_in_new</i>Encontros</a>'+
                    '</td></tr>');//cria a nova linha da tabela        
                var table = $('#table_projeto > tbody');//pega a tabela
                table.append(new_row);//adiciona a nova linha na tabela
                table = $('#table_projeto > tbody');
                var rows = $('tr',table); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toLowerCase();
                    var keyB = $(b).find('td:eq(1)').text().toLowerCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome
                rows.each(function (index,row) {
                    table.append(row);
                });//adiciona na tabela ordenadamente
                $('#modal_projeto_edit').modal('close');
                page.disable_buttons();
                page.getRow_selected().removeClass('selected');
                page.setRow_selected(null);
                row.remove();
                page.pagination($('#table_projeto').find('tr').get());
            }else{
                Materialize.toast('Falha ao modificar projeto.',4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao modificar projeto.',4000);
        });
        request.always(function () {
            $inputs.prop('disabled', false);
        });
    });

    /*  DELETE  */
    $('#bt_del_projeto').click(function(){
        $('#del_projeto_name').text($(page.getRow_selected()).find("td:eq(1)").text());
        $('#modal_projeto_delete').modal('open');
    });
    $('#bt_del_projeto_confirm').click(function(){
        var row =  page.getRow_selected();
        var id = row[0]['cells']['id']['innerText'];
        event.preventDefault();
        if (request) {
            request.abort();
        }
        request = $.ajax({
            url: 'projeto/delete',
            type: 'post',
            dataTye: 'json',
            data: {'idProjeto': id},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){
                row.remove();
                if(row.get(0) == page.getRow_selected().get(0)){
                    page.setRow_selected(null);
                    page.disable_buttons();
                }
                Materialize.toast('Projeto excluido com sucesso!', 4000);
                page.pagination($('#table_projeto').find('tr').get());
            }else{
               Materialize.toast('Falha ao deletar projeto.', 4000);
           }

       });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao deletar projeto.', 4000);
        });
    });

    /*  PROCURAR    */
    $("#field_search_projeto").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#bt_search_projeto").click();
        }
    });
    $('#bt_search_projeto').click(function(){
        var chave = $('#field_search_projeto').val();
        var rows = $('#table_projeto').find('tr').get();
        var matches = [];
        matches.push($(rows[0]));
        loop:
        for(var i = 1; i < rows.length; i++){
            var cols = $(rows[i]).find('td').get();
            for(var j = 0; j < cols.length; j++){
                if($(cols[j]).text().toLowerCase().indexOf(chave.toLowerCase()) != -1){
                    $(rows[i]).attr('hidden',false);
                    matches.push($(rows[i]));
                    continue loop;
                }
            }
            $(rows[i]).attr('hidden',true);
        }
        page.pagination($(matches));
    });
    /*  DETALHAR    */
    $('#bt_detail_projeto').click(function(){
        var row = page.getRow_selected();
        var cells = row[0]['cells'];
        request = $.ajax({
            url: 'projeto/find',
            type: 'post',
            data: {'idProjeto': cells[0]['innerText']},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            var form = $('#form_detail_projeto');
            var inputs_values = {
                'idProjeto': response.idProjeto,
                'tituloProjeto' : response.tituloProjeto,
                'descProjeto' : response.descProjeto,
                'dtInicio' : response.dtInicio,
                'dtFim' : response.dtFim,
                'status_idstatus' : response.nomeStatus,
                'aluno_idaluno' : response.nomeAluno,
                'professor_orientador1' : response.nomeProfessor1,
                'professor_orientador2' : response.nomeProfessor2,
            };
            form.find('input, select, button, textarea').val(function (index, value) {
            return inputs_values[this.name];
        });

        $('#modal_projeto_detail').modal('open');
        $('select').material_select();
        $('textarea').trigger('autoresize');
        Materialize.updateTextFields();

    });   



});
})();