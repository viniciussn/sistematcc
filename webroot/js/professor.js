/*----------------------------------------------------------------------------------
|                                       PROFESSOR                                  |
----------------------------------------------------------------------------------*/
(function (){
    var request;
    var page =  UTIL.crud_page('professor');

    /*  CADASTRO    */
    $('#form_cad_professor').submit(function(){
        event.preventDefault();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find('input, select, button, textarea');
        var serializedData = $form.serialize();
        $inputs.prop('disabled', true);
        request = $.ajax({
            url: 'professor/add',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){                
                Materialize.toast('Professor cadastrado com sucesso!', 4000);
                $form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.id+
                    '</td><td>'+response.nome+
                    '</td><td>'+response.email+
                    '</td><td>'+response.telefone+
                    '</td></tr>');//cria a nova linha da tabela        
                
                $('#table_professor').children('tbody').append(new_row);
                var rows = $('#table_professor tbody  tr').get(); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toUpperCase();
                    var keyB = $(b).find('td:eq(1)').text().toUpperCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome

                $.each(rows, function(index, row) {
                    $('#table_professor').children('tbody').append(row);
                });
                page.pagination($('#table_professor').find('tr').get());
            }else{
                Materialize.toast('Falha ao cadastrar professor.', 4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
         Materialize.toast('Falha ao cadastrar professor.', 4000);
     });
        request.always(function () {
            $inputs.prop('disabled', false);
        });
    });

    /*  EDITAR    */
    $('#bt_edit_professor').click(function(){
        var row = page.getRow_selected();
        var cells = row[0]['cells'];
        var form = $('#form_edit_professor');
        var inputs_values = {
            'idProfessor': cells[0]['innerText'],
            'nomeProfessor' : cells[1]['innerText'],
            'emailProfessor' : cells[2]['innerText'],
            'telefoneProfessor'   :cells [3]['innerText'],
        };
        form.find('input').val(function (index, value) {
            return inputs_values[this.name];
        });
        $('#modal_professor_edit').modal('open');
        Materialize.updateTextFields();
    });

    $('#form_edit_professor').submit(function(){
        event.preventDefault();
        var row = page.getRow_selected();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find('input, select, button, textarea');
        var serializedData = $form.serialize();
        $inputs.prop('disabled', true);
        request = $.ajax({
            url: 'professor/update',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result=='success'){
                Materialize.toast('Professor modificado com sucesso!',4000);
                $form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.id+
                    '</td><td>'+response.nome+
                    '</td><td>'+response.email+
                    '</td><td>'+response.telefone+
                    '</td></tr>');//cria a nova linha da tabela        
                var table = $('#table_professor > tbody');//pega a tabela
                table.append(new_row);//adiciona a nova linha na tabela
                table = $('#table_professor > tbody');
                var rows = $('tr',table); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toLowerCase();
                    var keyB = $(b).find('td:eq(1)').text().toLowerCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome
                rows.each(function (index,row) {
                    table.append(row);
                });//adiciona na tabela ordenadamente
                $('#modal_professor_edit').modal('close');
                page.disable_buttons();
                page.getRow_selected().removeClass('selected');
                page.setRow_selected(null);
                row.remove();
                page.pagination($('#table_professor').find('tr').get());
            }else{
                Materialize.toast('Falha ao modificar professor.',4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao modificar professor.',4000);
        });
        request.always(function () {
            $inputs.prop('disabled', false);
        });
    });

    /*  DELETE  */
    $('#bt_del_professor').click(function(){
        $('#del_professor_name').text($(page.getRow_selected()).find("td:eq(1)").text());
        $('#modal_professor_delete').modal('open');
    });
    $('#bt_del_professor_confirm').click(function(){
        var row =  page.getRow_selected();
        var id = row[0]['cells']['id']['innerText'];
        event.preventDefault();
        if (request) {
            request.abort();
        }
        request = $.ajax({
            url: 'professor/delete',
            type: 'post',
            dataTye: 'json',
            data: {'idProfessor': id},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){
                row.remove();
                if(row.get(0) == page.getRow_selected().get(0)){
                    page.setRow_selected(null);
                    page.disable_buttons();
                }
                Materialize.toast('Professor excluido com sucesso!', 4000);
                page.pagination($('#table_professor').find('tr').get());
            }else{
               Materialize.toast('Falha ao deletar professor.', 4000);
           }

       });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao deletar professor.', 4000);
        });
    });

    /*  PROCURAR    */
    $("#field_search_professor").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#bt_search_professor").click();
        }
    });
    $('#bt_search_professor').click(function(){
        var chave = $('#field_search_professor').val();
        var rows = $('#table_professor').find('tr').get();
        var matches = [];
        matches.push($(rows[0]));
        loop:
        for(var i = 1; i < rows.length; i++){
            var cols = $(rows[i]).find('td').get();
            for(var j = 0; j < cols.length; j++){
                if($(cols[j]).text().toLowerCase().indexOf(chave.toLowerCase()) != -1){
                    $(rows[i]).attr('hidden',false);
                    matches.push($(rows[i]));
                    continue loop;
                }
            }
            $(rows[i]).attr('hidden',true);
        }
        page.pagination($(matches));
    });
    /*  DETALHAR    */
    $('#bt_detail_professor').click(function(){
      var row = page.getRow_selected();
      var cells = row[0]['cells'];
      var form = $('#form_detail_professor');
      var inputs_values = {
        'nomeProfessor' : cells[1]['innerText'],
        'emailProfessor' : cells[2]['innerText'],
        'telefoneProfessor'   :cells [3]['innerText'],
    };
    form.find('input').val(function (index, value) {
        return inputs_values[this.name];
    });
    $('#modal_professor_detail').modal('open');
    Materialize.updateTextFields(); 
});
})();