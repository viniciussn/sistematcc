/*----------------------------------------------------------------------------------
|                                       ENCONTRO                                    |
----------------------------------------------------------------------------------*/
(function (){
    var request;
    /*  PROCURAR    */
    $("#field_search_encontro").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#bt_search_encontro").click();
        }
    });
 
    $('#bt_search_encontro').click(function(){
        var chave = $('#field_search_encontro').val();
        var rows = $('#table_encontro').find('tr').get();
        var matches = [];
        matches.push($(rows[0]));
        loop:
        for(var i = 1; i < rows.length; i++){
            var cols = $(rows[i]).find('td').get();
            for(var j = 0; j < cols.length; j++){
                if($(cols[j]).text().toLowerCase().indexOf(chave.toLowerCase()) != -1){
                    $(rows[i]).attr('hidden',false);
                    matches.push($(rows[i]));
                    continue loop;
                }
            }
            $(rows[i]).attr('hidden',true);
        }
        page.pagination($(matches));
    });

    /*  DETALHAR    */
    $('#bt_detail_encontro').click(function(){
        var row = page.getRow_selected();
        var cells = row[0]['cells'];
        var projeto_idProjeto = location.pathname.split('/')[3];
        request = $.ajax({
            url: '/encontro/find',
            type: 'post',
            data: {'idEncontro': cells[0]['innerText'],'projeto_idProjeto':projeto_idProjeto},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            var form = $('#form_detail_encontro');
            var inputs_values = {
                'idEncontro': response.idEncontro,
                'temaEncontro' : response.temaEncontro,
                'descEncontro' : response.descEncontro,
                'atividades' : response.atividades,
                'atividadesFuturas' : response.atividadesFuturas,
                'data' : response.data,
                'hora' : response.hora,
                'local' : response.local,
                'problemas' : response.problemas,
                'projeto' : response.nomeProjeto,
            };
            form.find('input, select, button, textarea').val(function (index, value) {
                return inputs_values[this.name];
            });

            $('#modal_encontro_detail').modal('open');
            $('select').material_select();
            $('textarea').trigger('autoresize');
            Materialize.updateTextFields();

        });   



    });
})();