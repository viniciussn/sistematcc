/*----------------------------------------------------------------------------------
|                                       ETAPA                                    |
----------------------------------------------------------------------------------*/
(function (){
    var request;
    var page =  UTIL.crud_page('etapa');

    /*  CADASTRO    */
    $('#form_cad_etapa').submit(function(){
        event.preventDefault();
        if (request) {
            request.abort();
        }
        var form = $(this);
        var inputs = form.find('input, select, button, textarea');      
        var serializedData = form.serialize();
        var projeto_idProjeto = location.pathname.split('/')[3];
        serializedData+='&projeto_idProjeto='+projeto_idProjeto;
        inputs.prop('disabled', true);
        request = $.ajax({
            url: '/etapa/add',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){                
                Materialize.toast('Etapa cadastrado com sucesso!', 4000);
                form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.idEtapa+
                    '</td><td>'+response.nomeEtapa+
                    '</td><td>'+response.dtInicio+
                    '</td><td>'+response.status+
                    '</td></tr>');//cria a nova linha da tabela        
                
                $('#table_etapa').children('tbody').append(new_row);
                var rows = $('#table_etapa tbody  tr').get(); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toUpperCase();
                    var keyB = $(b).find('td:eq(1)').text().toUpperCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome

                $.each(rows, function(index, row) {
                    $('#table_etapa').children('tbody').append(row);
                });
                page.pagination($('#table_etapa').find('tr').get());
            }else{
                Materialize.toast('Falha ao cadastrar etapa.', 4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
         Materialize.toast('Falha ao cadastrar etapa.', 4000);
     });
        request.always(function () {
            inputs.prop('disabled', false);
        });
    });

    /*  EDITAR    */
    $('#bt_edit_etapa').click(function(){
        var row = page.getRow_selected();
        var cells = row[0]['cells'];  
        var projeto_idProjeto = location.pathname.split('/')[3];      
        request = $.ajax({
            url: '/etapa/find',
            type: 'post',
            data: {'idEtapa': cells[0]['innerText'],'projeto_idProjeto':projeto_idProjeto},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            var form = $('#form_edit_etapa');
            var inputs_values = {
                'idEtapa': response.idEtapa,
                'nomeEtapa' : response.nomeEtapa,
                'descEtapa' : response.descEtapa,
                'comentarioEtapa' : response.comentarioEtapa,
                'dtInicio' : response.dtInicio,
                'dtFim' : response.dtFim,
                'statusEtapa_idStatus' : response.statusEtapa_idStatus,
                'projeto' : response.nomeProjeto,
            };
            console.log(inputs_values);

            form.find('input, select, button, textarea').val(function (index, value) {
                return inputs_values[this.name];
            });

            $('#modal_etapa_edit').modal('open');
            $('select').material_select();
            Materialize.updateTextFields();

        });
    });
    //salvar
    $('#form_edit_etapa').submit(function(){
        event.preventDefault();
        var row = page.getRow_selected();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find('input, select, button, textarea');
        var serializedData = $form.serialize();
        console.log(serializedData);

        $inputs.prop('disabled', true);
        request = $.ajax({
            url: '/etapa/update',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result=='success'){
                Materialize.toast('Etapa modificado com sucesso!',4000);
                $form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.idEtapa+
                    '</td><td>'+response.nomeEtapa+
                    '</td><td>'+response.dtInicio+
                    '</td><td>'+response.status+
                    '</td></tr>');//cria a nova linha da tabela        
                var table = $('#table_etapa > tbody');//pega a tabela
                table.append(new_row);//adiciona a nova linha na tabela
                table = $('#table_etapa > tbody');
                var rows = $('tr',table); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toLowerCase();
                    var keyB = $(b).find('td:eq(1)').text().toLowerCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome
                rows.each(function (index,row) {
                    table.append(row);
                });//adiciona na tabela ordenadamente
                $('#modal_etapa_edit').modal('close');
                page.disable_buttons();
                page.getRow_selected().removeClass('selected');
                page.setRow_selected(null);
                row.remove();
                page.pagination($('#table_etapa').find('tr').get());
            }else{
                Materialize.toast('Falha ao modificar etapa.',4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao modificar etapa.',4000);
        });
        request.always(function () {
            $inputs.prop('disabled', false);
        });
    });

    /*  DELETE  */
    $('#bt_del_etapa').click(function(){
        $('#del_etapa_name').text($(page.getRow_selected()).find("td:eq(1)").text());
        $('#modal_etapa_delete').modal('open');
    });
    $('#bt_del_etapa_confirm').click(function(){
        var row =  page.getRow_selected();
        var id = row[0]['cells']['id']['innerText'];
        event.preventDefault();
        if (request) {
            request.abort();
        }
        request = $.ajax({
            url: '/etapa/delete',
            type: 'post',
            dataTye: 'json',
            data: {'idEtapa': id},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){
                row.remove();
                if(row.get(0) == page.getRow_selected().get(0)){
                    page.setRow_selected(null);
                    page.disable_buttons();
                }
                Materialize.toast('Etapa excluido com sucesso!', 4000);
                page.pagination($('#table_etapa').find('tr').get());
            }else{
               Materialize.toast('Falha ao deletar etapa.', 4000);
           }

       });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao deletar etapa.', 4000);
        });
    });

    /*  PROCURAR    */
    $("#field_search_etapa").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#bt_search_etapa").click();
        }
    });
    $('#bt_search_etapa').click(function(){
        var chave = $('#field_search_etapa').val();
        var rows = $('#table_etapa').find('tr').get();
        var matches = [];
        matches.push($(rows[0]));
        loop:
        for(var i = 1; i < rows.length; i++){
            var cols = $(rows[i]).find('td').get();
            for(var j = 0; j < cols.length; j++){
                if($(cols[j]).text().toLowerCase().indexOf(chave.toLowerCase()) != -1){
                    $(rows[i]).attr('hidden',false);
                    matches.push($(rows[i]));
                    continue loop;
                }
            }
            $(rows[i]).attr('hidden',true);
        }
        page.pagination($(matches));
    });
    /*  DETALHAR    */
    $('#bt_detail_etapa').click(function(){
        var row = page.getRow_selected();
        var cells = row[0]['cells'];
        var projeto_idProjeto = location.pathname.split('/')[3];        
        request = $.ajax({
            url: '/etapa/find',
            type: 'post',
            data: {'idEtapa': cells[0]['innerText'],'projeto_idProjeto':projeto_idProjeto},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            var form = $('#form_detail_etapa');
            var inputs_values = {
                'idEtapa': response.idEtapa,
                'nomeEtapa' : response.nomeEtapa,
                'descEtapa' : response.descEtapa,
                'comentarioEtapa' : response.comentarioEtapa,
                'dtInicio' : response.dtInicio,
                'dtFim' : response.dtFim,
                'status' : response.nomeStatus,
                'projeto' : response.nomeProjeto,
            };
            form.find('input, select, button, textarea').val(function (index, value) {
                return inputs_values[this.name];
            });

            $('#modal_etapa_detail').modal('open');
            $('select').material_select();
            $('textarea').trigger('autoresize');
            Materialize.updateTextFields();

        });   



    });
})();