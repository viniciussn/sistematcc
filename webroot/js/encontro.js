/*----------------------------------------------------------------------------------
|                                       ETAPA                                    |
----------------------------------------------------------------------------------*/
(function (){
    var request;
    var page =  UTIL.crud_page('encontro');

    /*  CADASTRO    */
    $('#form_cad_encontro').submit(function(){
        event.preventDefault();
        if (request) {
            request.abort();
        }
        var form = $(this);
        var inputs = form.find('input, select, button, textarea');      
        var serializedData = form.serialize();
        var projeto_idProjeto = location.pathname.split('/')[3];
        serializedData+='&projeto_idProjeto='+projeto_idProjeto;
        inputs.prop('disabled', true);
        request = $.ajax({
            url: '/encontro/add',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){                
                Materialize.toast('Encontro cadastrado com sucesso!', 4000);
                form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.idEncontro+
                    '</td><td>'+response.temaEncontro+
                    '</td><td>'+response.data+
                    '</td><td>'+response.hora+
                    '</td><td>'+response.local+
                    '</td></tr>');//cria a nova linha da tabela        
                
                $('#table_encontro').children('tbody').append(new_row);
                var rows = $('#table_encontro tbody  tr').get(); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toUpperCase();
                    var keyB = $(b).find('td:eq(1)').text().toUpperCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome

                $.each(rows, function(index, row) {
                    $('#table_encontro').children('tbody').append(row);
                });
                page.pagination($('#table_encontro').find('tr').get());
            }else{
                Materialize.toast('Falha ao cadastrar encontro.', 4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
           Materialize.toast('Falha ao cadastrar encontro.', 4000);
       });
        request.always(function () {
            inputs.prop('disabled', false);
        });
    });

    /*  EDITAR    */
    $('#bt_edit_encontro').click(function(){
        var row = page.getRow_selected();
        var cells = row[0]['cells'];  
        var projeto_idProjeto = location.pathname.split('/')[3];        
        request = $.ajax({
            url: '/encontro/find',
            type: 'post',
            data: {'idEncontro': cells[0]['innerText'],'projeto_idProjeto':projeto_idProjeto},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            var form = $('#form_edit_encontro');
            var inputs_values = {
              'idEncontro': response.idEncontro,
              'temaEncontro' : response.temaEncontro,
              'descEncontro' : response.descEncontro,
              'atividades' : response.atividades,
              'atividadesFuturas' : response.atividadesFuturas,
              'data' : response.data,
              'hora' : response.hora,
              'local' : response.local,
              'problemas' : response.problemas,
              'projeto_idProjeto' : response.projeto_idProjeto,
          };


          form.find('input, select, button, textarea').val(function (index, value) {
            return inputs_values[this.name];
        });

          $('#modal_encontro_edit').modal('open');
          $('select').material_select();
          Materialize.updateTextFields();

      });
    });
    //salvar
    $('#form_edit_encontro').submit(function(){
        event.preventDefault();
        var row = page.getRow_selected();
        if (request) {
            request.abort();
        }
        var $form = $(this);
        var $inputs = $form.find('input, select, button, textarea');
        var serializedData = $form.serialize();
        console.log(serializedData);

        $inputs.prop('disabled', true);
        request = $.ajax({
            url: '/encontro/update',
            type: 'post',
            data: serializedData,
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result=='success'){
                Materialize.toast('Encontro modificado com sucesso!',4000);
                $form[0].reset();//reseta o form
                var new_row = $('<tr><td name="id" style="display:none;">'+response.idEncontro+
                    '</td><td>'+response.temaEncontro+
                    '</td><td>'+response.data+
                    '</td><td>'+response.hora+
                    '</td><td>'+response.local+
                    '</td></tr>');//cria a nova linha da tabela        
                var table = $('#table_encontro > tbody');//pega a tabela
                table.append(new_row);//adiciona a nova linha na tabela
                table = $('#table_encontro > tbody');
                var rows = $('tr',table); 
                rows.sort(function (a,b) {
                    var keyA = $(a).find('td:eq(1)').text().toLowerCase();
                    var keyB = $(b).find('td:eq(1)').text().toLowerCase();
                    return (keyA>keyB) ? 1 : 0;
                });//ordena as linhas conforme o nome
                rows.each(function (index,row) {
                    table.append(row);
                });//adiciona na tabela ordenadamente
                $('#modal_encontro_edit').modal('close');
                page.disable_buttons();
                page.getRow_selected().removeClass('selected');
                page.setRow_selected(null);
                row.remove();
                page.pagination($('#table_encontro').find('tr').get());
            }else{
                Materialize.toast('Falha ao modificar encontro.',4000);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao modificar encontro.',4000);
        });
        request.always(function () {
            $inputs.prop('disabled', false);
        });
    });

    /*  DELETE  */
    $('#bt_del_encontro').click(function(){
        $('#del_encontro_name').text($(page.getRow_selected()).find("td:eq(1)").text());
        $('#modal_encontro_delete').modal('open');
    });
    $('#bt_del_encontro_confirm').click(function(){
        var row =  page.getRow_selected();
        var id = row[0]['cells']['id']['innerText'];
        event.preventDefault();
        if (request) {
            request.abort();
        }
        request = $.ajax({
            url: '/encontro/delete',
            type: 'post',
            dataTye: 'json',
            data: {'idEncontro': id},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            if(response.result == 'success'){
                row.remove();
                if(row.get(0) == page.getRow_selected().get(0)){
                    page.setRow_selected(null);
                    page.disable_buttons();
                }
                Materialize.toast('Encontro excluido com sucesso!', 4000);
                page.pagination($('#table_encontro').find('tr').get());
            }else{
             Materialize.toast('Falha ao deletar encontro.', 4000);
         }

     });
        request.fail(function (jqXHR, textStatus, errorThrown){
            Materialize.toast('Falha ao deletar encontro.', 4000);
        });
    });

    /*  PROCURAR    */
    $("#field_search_encontro").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#bt_search_encontro").click();
        }
    });
    $('#bt_search_encontro').click(function(){
        var chave = $('#field_search_encontro').val();
        var rows = $('#table_encontro').find('tr').get();
        var matches = [];
        matches.push($(rows[0]));
        loop:
        for(var i = 1; i < rows.length; i++){
            var cols = $(rows[i]).find('td').get();
            for(var j = 0; j < cols.length; j++){
                if($(cols[j]).text().toLowerCase().indexOf(chave.toLowerCase()) != -1){
                    $(rows[i]).attr('hidden',false);
                    matches.push($(rows[i]));
                    continue loop;
                }
            }
            $(rows[i]).attr('hidden',true);
        }
        page.pagination($(matches));
    });
    /*  DETALHAR    */
    $('#bt_detail_encontro').click(function(){
        var row = page.getRow_selected();
        var cells = row[0]['cells'];
        var projeto_idProjeto = location.pathname.split('/')[3];
        request = $.ajax({
            url: '/encontro/find',
            type: 'post',
            data: {'idEncontro': cells[0]['innerText'],'projeto_idProjeto':projeto_idProjeto},
            timeout: 20000
        });
        request.done(function (response, textStatus, jqXHR){
            var form = $('#form_detail_encontro');
            var inputs_values = {
                'idEncontro': response.idEncontro,
                'temaEncontro' : response.temaEncontro,
                'descEncontro' : response.descEncontro,
                'atividades' : response.atividades,
                'atividadesFuturas' : response.atividadesFuturas,
                'data' : response.data,
                'hora' : response.hora,
                'local' : response.local,
                'problemas' : response.problemas,
                'projeto' : response.nomeProjeto,
            };
            form.find('input, select, button, textarea').val(function (index, value) {
                return inputs_values[this.name];
            });

            $('#modal_encontro_detail').modal('open');
            $('select').material_select();
            $('textarea').trigger('autoresize');
            Materialize.updateTextFields();

        });   



    });
})();